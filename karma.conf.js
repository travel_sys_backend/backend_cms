var webpackConfig = require('./webpack.config.js');
module.exports = function (config) {
  config.set({
    browsers: ['Chrome'], //browers to use for testng
    singleRun: true,
    frameworks: ['mocha'], //frameworks used in testing
    files: [
    'node_modules/jquery/dist/jquery.min.js',
    'node_modules/foundation-sites/dist/foundation.min.js',
    'app/tests/**/*.test.jsx'
    ], //globbing pattern
    preprocessors: {
      'app/tests/**/*.test.jsx':['webpack', 'sourcemap']
    },
    reporters: ['mocha'],
    client: {
      mocha: {
        timeout: '5000'
      }
    },
    webpack:webpackConfig,
    webpackServer: {
      noInfo: true
    },
    /*Set the timeout to 30 seconds to wait for the bundle to be built*/
    browserNoActivityTimeout: 30000
  });
}
