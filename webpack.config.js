'use strict'
var webpack = require('webpack');
var path = require('path');
//plugin for environment variables
var envFile = require('node-env-file');
//import compression plugin to compress to gzip
var CompressionPlugin = require('compression-webpack-plugin');
//enviroment variable
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

//check for which enviroment file to use and protect error thrown in production
try {
  envFile(path.join(__dirname, 'config/'+process.env.NODE_ENV + '.env'));
} catch (e) {

}

module.exports = {
  //webpack reads our raw source from here
  context: __dirname + '/app', //root of our code files
  entry: {
    jquery: 'script!jquery/dist/jquery.min.js',
    foundation: 'script!foundation-sites/dist/foundation.min.js',
    app: './app.jsx'
  },
  externals: {
    jQuery: 'jQuery'
  },
  //configure global imports
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(process.env.NODE_ENV),
        API_KEY: JSON.stringify(process.env.API_KEY),
        AUTH_DOMAIN: JSON.stringify(process.env.AUTH_DOMAIN),
        DATABASE_URL: JSON.stringify(process.env.DATABASE_URL),
        STORAGE_BUCKET: JSON.stringify(process.env.STORAGE_BUCKET),
        BONSAI_URL: JSON.stringify(process.env.BONSAI_URL),
        FB_NAME: JSON.stringify(process.env.FB_NAME),
        FB_TOKEN: JSON.stringify(process.env.FB_TOKEN),
      }
    }),
    new webpack.LoaderOptionsPlugin({
        test: /\.scss$/,
        options: {
          sassLoader: {
            includePaths: [
              path.resolve(__dirname, './node_modules/foundation-sites/scss')
            ]
          }
        }
    })
  ],
  //the transpiled output is here
  output: {
    path: __dirname + '/public/assets',
    filename: '[name].bundle.js',
    publicPath: '/assets', // for the dev server
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/, //check for all js files
        use: [{
          loader: 'babel-loader',
          options: { presets: ['react', 'es2015', 'stage-0'] }
        }],
        exclude: /(node_modules)/ //exclude this modules from being transpiled
      },
      {
        test: /\.(png|jpg|jpeg|gif|woff)$/,
        loader: 'url-loader'
      }
    ]
  },
  //finding app modules
  resolve: {
    modules: [
      "node_modules",
      "./app/components",
      "./app/components/forms",
      "./app/components/pages",
      "./app/components/sections"
    ],
    extensions: [" ",".js", ".jsx"],
    alias: {
      app: path.resolve(__dirname, "app/"),
      applicationStyles$: path.resolve(__dirname,  "app/styles/app.scss"),
    },
  },
  devServer: {
    contentBase: __dirname + '/app',
    headers: { 'Access-Control-Allow-Origin': '*' }
  },
  /*only load the source maps if not production*/
  devtool: process.env.NODE_ENV === 'production' ? undefined : 'cheap-module-eval-source-map'
};
