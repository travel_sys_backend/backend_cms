export var supplierReducer = (state = [], action) =>  {
  switch (action.type) {
    case 'ADD_SUPPLIER':
      return[
        ...state,
        action.supplier
      ];
    case 'LOAD_SUPPLIERS':
      return[
        ...state,
        ...action.suppliers
      ];
    case 'UPDATE_SUPPLIER':
      return state.map((supplier) => {
        if(supplier.id === action.id) {
          return {
            ...supplier,
            ...action.updates
          };
        } else {
          return supplier;
        }
      });
    case 'DELETE_SUPPLIER':
      return state.filter((supplier) => {
        return supplier.id !== action.key;
      });
    default:
      return state;
  }
};

export var serviceLineReducer = (state = [], action) => {
  switch (action.type) {
    case 'ADD_SUPPLIER_SERVICE_LINE':
      return[
        ...state,
        action.serviceLine
      ];
      case 'UPDATE_SERVICE_LINE':
      return state.map((sl) => {
        if(sl.id === action.id) {
          return {
            ...sl,
            ...action.updates
          };
        } else {
          return sl;
        }
      });
      case 'DELETE_SERVICE_LINE':
        return state.filter((sl) => {
          /*check if the id of service line is to be deleted or not*/
          return sl.id !== action.id;
        });
    default:
      return state;
  }
};

export var currSupplierReducer = (state = '', action) => {
  switch(action.type) {
    case 'SET_CURR_SUPPLIER':
      return action.key;
    default:
      return state;
  };
}

export var policyReducer = (state =  [], action) => {
  switch(action.type) {
    case 'ADD_SUPPLIER_POLICY':
      return [
        ...state,
        action.policy
      ];
    case 'UPDATE_POLICY':
      /*Iterate through each element in the set*/
      return state.map((policy) => {
        /*check if element is the one to be updated*/
        if(policy.id === action.id) {
          /*Apply updates*/
          return {
            ...policy,
            ...action.updates
          };
          /*Not element just return it*/
        } else {
          return policy;
        }
      });
    case 'DELETE_POLICY':
      return state.filter((policy) => {
        /*return only element that dont match delete criterion*/
        return policy.id !== action.id
      })
    default:
      return state;
  };
}

export var ratePeriodsReducer =  (state = [], action) => {
  switch(action.type) {
    case 'ADD_SUPPLIER_RATE_PERIOD':
      return [
        ...state,
        action.period
      ];
    case 'DELETE_RATE_PERIOD':
      return state.filter((period) => {
        return period.id !== action.id;
      });
    case 'UPDATE_RATE_PERIOD':
      return state.map((period) => {
        if(period.id === action.id) {
          return {
            ...period,
            ...action.updates
          }
        } else {
          return period;
        }
      });
    default:
        return state;
  };
}

export var rateCostsReducer = (state = [], action) => {
  switch(action.type) {
    case 'ADD_SUPPLIER_COSTS':
      return [
        ...state,
        action.ratecost
      ];
    case 'UPDATE_COST':
      return state.map((cost) => {
        if(cost.id === action.id) {
          return {
            ...cost,
            ...action.updates
          }
        } else {
          return cost;
        }
      });
    case 'DELETE_COST':
      return state.filter((cost) => {
        return cost.id !== action.id;
      });
    default:
        return state;
  }
}
