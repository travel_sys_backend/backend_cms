import * as redux from 'redux';
import thunk from 'redux-thunk';

//import reducers here
import  {supplierReducer, currSupplierReducer, serviceLineReducer,
  policyReducer, ratePeriodsReducer, rateCostsReducer} from 'app/reducers/reducers';

export var configure = (initialState = {}) => {
    var reducer = redux.combineReducers({
      suppliers: supplierReducer,
      currentSupplier: currSupplierReducer,
      serviceLines: serviceLineReducer,
      policies: policyReducer,
      rateperiods: ratePeriodsReducer,
      ratecosts: rateCostsReducer
    });

    var store = redux.createStore(reducer, initialState, redux.compose(
        redux.applyMiddleware(thunk),
        window.devToolsExtension ? window.devToolsExtension() : f  => f
    ));

    return store;
};
