var React = require('react');
var ReactDOM = require('react-dom');
//add connect and actions
var {connect} = require('react-redux');
var actions = require('app/actions/actions');
import ProductPeriodList from 'ProductPeriodList';

var RatePeriodsForm = React.createClass({
    submitRP: function(e) {
      e.preventDefault();
      var {dispatch, currentSupplier} = this.props;
      //construct
      var formRef = this.refs;
      var name = formRef.rate_period_name.value;
      var start =  formRef.rate_period_start.value;
      var end  =  formRef.rate_period_end.value;

      //validate and send
      if(name.length > 0 && start.length > 0 && end.length > 0) {
        dispatch(actions.startPeriod(name,start,end, currentSupplier));
        ReactDOM.findDOMNode(this.refs.form).reset();
      }
    },
    render: function() {
       return(
               <div>
                 <div className="row">
                   <div className="small-12 medium-6 large-6 columns">
                     <form ref="form" onSubmit={this.submitRP}>
                       <div className="service-line-form-group">
                         <label className="rates-f-label">NAME: </label>
                         <input type="text" className="rates-f-input" ref="rate_period_name"/>
                       </div>
                       <div className="service-line-form-group">
                         <label className="rates-f-label">START DATE:</label>
                         <input type="text" className="rates-f-input" ref="rate_period_start"/>
                       </div>
                       <div className="service-line-form-group">
                         <label className="rates-f-label">END DATE:</label>
                         <input type="text" className="rates-f-input" ref="rate_period_end"/>
                       </div>
                       <div className="service-line-form-group">
                         <button className="sls-f-submit button">SET RATE PERIOD</button>
                       </div>
                     </form>
                   </div>
                   <div className="small-12 medium-6 large-6 columns">
                     <ProductPeriodList/>
                   </div>
                 </div>
               </div>
             );
    }
});
/*pick needed pieces of state*/
export default connect((state) => {
  return {
    currentSupplier: state.currentSupplier
  }
})(RatePeriodsForm)
