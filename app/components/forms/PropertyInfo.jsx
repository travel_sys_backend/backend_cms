var React = require('react');
var ReactDOM = require('react-dom');
//Add Redux props to use
var {connect} = require('react-redux');
var actions = require('app/actions/actions');

export var PropertyInfo = React.createClass({
    /*add a handler to  deal with submission and validation of data*/
    submitSupplier: function(e){
      e.preventDefault();
      var {dispatch} = this.props;

      /*data fields*/
      var formRef = this.refs;

      var name  = formRef.supplier_name.value;
      var fact_sheet = formRef.supplier_fact_sheet.value;
      var liability_sheet = formRef.supplier_liability_sheet.value;
      var group = formRef.supplier_commission.value;
      var commission = formRef.supplier_commission.value;
      var country = formRef.supplier_country.value;
      var region = formRef.supplier_region.value;
      var email = formRef.supplier_email.value;
      var telephone = formRef.supplier_phone.value;
      var website = formRef.supplier_website.value;
      var category = formRef.supplier_category.value;
      var rating = formRef.supplier_rating.value;

      if(name.length > 0  && fact_sheet.length > 0 && liability_sheet.length > 0 && group.length > 0 && commission.length > 0) {
        dispatch(actions.startAddSupplier(name, fact_sheet,liability_sheet,group,commission,country,region,email,telephone, website,category,rating));
        ReactDOM.findDOMNode(this.refs.supplier_form).reset();

      }
      else {
        formRef.supplier_name.focus();
      }

    },
    render: function() {
       return(
               <div>
                 <form ref="supplier_form" onSubmit={this.submitSupplier}>
                   <div className="small-12 medium-5 large-5 columns">
                     <div className="rates-form-group">
                       <label className="rates-f-label">NAME:</label>
                       <input className="rates-f-input" type="text" ref="supplier_name"/>
                     </div>
                     <div className="rates-form-group">
                       <label className="rates-f-label">COMMISSION:</label>
                       <input className="rates-f-input" type="text" ref="supplier_commission"/>
                     </div>
                     <div className="rates-form-group">
                       <label className="rates-f-label">FACT SHEET:</label>
                       <textarea className="rates-f-input" rows="10" ref="supplier_fact_sheet"></textarea>
                     </div>
                     <div className="rates-form-group">
                       <label className="rates-f-label">LIABILITY:</label>
                       <textarea className="rates-f-input" rows="10" ref="supplier_liability_sheet"></textarea>
                     </div>
                     <div className="rates-form-group">
                       <label className="rates-f-label">GROUP:</label>
                       <input className="rates-f-input" type="text" ref="supplier_group"/>
                     </div>
                     <div className="rates-form-group">
                       <label className="rates-f-label">COUNTRY:</label>
                       <input className="rates-f-input" type="text" ref="supplier_country"/>
                     </div>
                     <div className="rates-form-group">
                       <label className="rates-f-label">CITY/TOWN:</label>
                       <input className="rates-f-input" type="text" ref="supplier_region"/>
                     </div>
                   </div>
                   <div className="small-12 medium-5 large-5 columns">
                     <div className="rates-form-group">
                       <label className="rates-f-label">EMAIL:</label>
                       <input className="rates-f-input" type="text" ref="supplier_email"/>
                     </div>
                     <div className="rates-form-group">
                       <label className="rates-f-label">TELEPHONE:</label>
                       <input className="rates-f-input" type="text" ref="supplier_phone"/>
                     </div>
                     <div className="rates-form-group">
                       <label className="rates-f-label">WEBSITE:</label>
                       <input className="rates-f-input" type="text" ref="supplier_website"/>
                     </div>
                     <div className="rates-form-group">
                       <label className="rates-f-label">CATEGORY:</label>
                       <input className="rates-f-input" type="text" ref="supplier_category"/>
                     </div>
                     <div className="rates-form-group">
                       <label className="rates-f-label">RATING:</label>
                       <input className="rates-f-input" type="text" ref="supplier_rating"/>
                     </div>
                     <div className="rates-form-group">
                       <button type="submit" className="rates-f-submit button">SAVE CHANGES</button>
                     </div>
                   </div>
                   <div className="small-12 medium-2 large-2 columns">
                     <div className="image-div">
                       <img className="supplier-image" />
                     </div>
                     <button className="button sl-rates-image-button">UPLOAD IMAGE</button>
                   </div>
                 </form>
               </div>
             );
    }
});
export default connect()(PropertyInfo);
