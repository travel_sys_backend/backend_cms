var React = require('react');

var SignUpForm = React.createClass({
    render: function() {
       return(
               <div className="sign-up-form">
                   <div className="sign-up-component">
                   <p className="form-title">SIGN UP TO JETS</p>
                   <form className="signupform">
                      <div className="row">
                       <div className="small-12 medium-4 large-4 columns">
                         <label className="text-label">FIRST</label>
                         <input type="text" ref="signupFirstname"/>
                         <label className="text-label">EMAIL</label>
                         <input type="text" ref="signupEmail"/>
                         <label className="text-label">USERNAME</label>
                         <input type="text" ref="signupUsername"/>
                       </div>
                       <div className="small-12 medium-4 large-4 columns">
                         <label className="text-label">LAST</label>
                         <input type="text" ref="signupLastname"/>
                         <label className="text-label">COMPANY</label>
                         <input type="text" ref="signupCompany"/>
                         <label className="text-label">PASSWORD</label>
                         <input type="password" ref="signupPassword"/>
                       </div>
                       <div className="confirm-password small-12 medium-4 large-4 columns">
                         <label className="text-label">CONFIRM PASSWORD</label>
                         <input type="password"  ref="signUpconfirmpassword"/>
                       </div>
                     </div>
                     <div className="row button-section small-12 medium-4 large-3 columns">
                       <button className="button signup-button">SIGN UP</button>
                     </div>
                   </form>
                 </div>
               </div>
             );
    }
});
module.exports =  SignUpForm;
