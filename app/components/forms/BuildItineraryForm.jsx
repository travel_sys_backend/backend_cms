var React = require('react');

var BuildItineraryForm = React.createClass({
    render: function() {
       return(
               <div className="build-form">
                 <form className="f-form">
                   <div className="build-form-group">
                     <label className="build-form-label">DURATION:</label>
                     <input type="text"/>
                   </div>
                   <div className="build-form-group">
                    <label className="build-form-label">CATEGORY:</label>
                    <input type="text"/>
                    </div>
                   <div className="build-form-group">
                     <label className="build-form-label">COUNTRY 1:</label>
                     <input type="text"/>
                   </div>
                   <div className="build-form-group">
                     <label className="build-form-label">REGION 1:</label>
                     <input type="text"/>
                   </div>
                   <div className="build-form-group">
                     <img className="build-form-image"/>
                     <div className="up-bt-div text-center">
                       <button className="build-image-upload button">UPLOAD IMAGE</button>
                       <p className="pixels">600x600 pixels</p>
                     </div>
                   </div>
                   <div className="build-form-group">
                     <button className="build-form-submit button">CREATE</button>
                   </div>
                 </form>
               </div>
             );
    }
});
module.exports =  BuildItineraryForm;
