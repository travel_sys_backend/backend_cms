var React = require('react');
//otherModal
var PasswordChangeSuccessModal = require('PasswordChangeSuccessModal');

var ChangePasswordModal = React.createClass({
    getInitialState: function() {
      return {
        showSuccessModal: false
      };
    },
    handleUpdate: function(e){
      e.preventDefault();
      this.setState({
        showSuccessModal: true
      });
    },
    componentDidMount: function () {
      var modal = new Foundation.Reveal($('#change-password-modal'));
      modal.open();
    },
    render: function() {
      var {showSuccessModal} = this.state;
      function renderSuccess (){
        if(showSuccessModal){
          return(
            <PasswordChangeSuccessModal/>
          );
        }
      }
       return(
               <div id="change-password-modal" className="reveal small text-center" data-reveal>
                 <p className="change-password-title">CHANGE PASSWORD</p>
                 <form>
                   <div className="form-group">
                     <label className="change-label">OLD PASSWORD</label>
                     <input className="change-input" type="password" ref="oldPassword"/>
                   </div>
                   <div className="form-group">
                     <label className="change-label">NEW PASSWORD</label>
                     <input className="change-input" type="password" ref="newPassword"/>
                   </div>
                   <div className="form-group">
                     <label className="change-label">CONFIRM PASSWORD</label>
                     <input className="change-input" type="password" ref="confirmPassword" />
                   </div>
                   <div>
                     <button className="button change-password-button" onClick={this.handleUpdate}>CHANGE PASSWORD</button>
                   </div>
                 </form>
                 {renderSuccess()}
               </div>
             );
    }
});
module.exports =  ChangePasswordModal;
