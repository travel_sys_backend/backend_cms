var React = require('react');
var {Link} = require('react-router');

var ProductLinks = React.createClass({
    render: function() {
       return(
               <div className="product-links">
                  <Link to="/product/primary" className="bt-p-link button">Search</Link>
                  <Link to="/product/rates" className="bt-p-link button">Add Service Line</Link>
                  <Link to="/product/build" className="bt-build button">Build Itinerary</Link>
               </div>
             );
    }
});
module.exports =  ProductLinks;
