var React = require('react');
var ReactDOM= require('react-dom');
//add connect and actions
var {connect} = require('react-redux');
var actions = require('app/actions/actions');
import AddRatesServiceLines from 'AddRatesServiceLines';
//normal export for testing
export var ServiceLineForm = React.createClass({
    /*Add Service line handler*/
    submitSL: function(e) {
      e.preventDefault();
      var {dispatch, currentSupplier} = this.props;
      //construct
      var formRef = this.refs;
      var name = formRef.service_line_name.value;
      var category = formRef.service_line_category.value;

      if(name.length > 0 && category.length > 0) {
        dispatch(actions.startServiceLine(name, category, currentSupplier));
        this.refs.form.getDOMNode().reset();
      }
    },
    render: function() {
       return(
               <div>
                <div className="small-12 medium-6 large-6 columns">
                  <form onSubmit={this.submitSL} ref="form">
                  <div className="service-line-form-group">
                    <label className="rates-f-label">NAME: </label>
                    <input type="text" className="rates-f-input" ref="service_line_name"/>
                  </div>
                  <div className="service-line-form-group">
                    <label className="rates-f-label">CATEGORY:</label>
                    <input type="text" className="rates-f-input" ref="service_line_category"/>
                  </div>
                  <div className="service-line-form-group">
                    <label className="rates-f-label">IMAGE:</label>
                    <button className="button rates-f-button">Upload File</button>
                  </div>
                  <div className="service-line-form-group">
                    <button className="sls-f-submit button">ADD SERVICE LINE</button>
                  </div>
                </form>
                </div>
                <div className="small-12 medium-6 large-6 columns">
                  <AddRatesServiceLines/>
                </div>
               </div>
             );
    }
});
/*pick the  state items needed to save sls*/
export default connect((state) => {
  return{
    currentSupplier: state.currentSupplier
  }
})(ServiceLineForm);
