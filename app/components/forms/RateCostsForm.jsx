var React = require('react');
var ReactDOM = require('react-dom');
//add connect and actions
var {connect} = require('react-redux');
var actions = require('app/actions/actions');
import ProductCostList from 'ProductCostList';

var RateCostsForm = React.createClass({
    submitRC: function(e) {
      e.preventDefault();
      var {dispatch, currentSupplier} =  this.props;

      var formRef = this.refs;
      var sl = formRef.rate_cost_service_line.value;
      var rp = formRef.rate_cost_rate_period.value;
      var type = formRef.rate_cost_type.value;
      var pps =  formRef.rate_cost_pps.value;
      var single =  formRef.rate_cost_single.value;
      var thirdadu = formRef.rate_cost_third_adult.value;
      var child = formRef.rate_cost_child.value;

      if(sl.length > 0 && rp.length > 0 && type.length > 0 && pps.length > 0 && single.length > 0 && thirdadu.length > 0 && child.length > 0) {
        dispatch(actions.startRateCost(sl,rp, type,pps,single,thirdadu, child, currentSupplier));
        ReactDOM.findDOMNode(this.refs.form).reset();
      };
    },
    render: function() {
      /*TODO Load Rate periods and service lines from state*/
      //render select items
      var {servicelines, rateperiods} = this.props;
      var option_render = (sl) => {
        return <option value={sl.id}>{sl.name}</option>
      }


       return(
               <div>
                <div className="small-12 medium-6 large-6 columns">
                  <form ref="form" onSubmit={this.submitRC}>
                  <div className="service-line-form-group">
                    <label className="rates-f-label">SERVICE LINE: </label>
                    <select ref="rate_cost_service_line">{servicelines.map(option_render)}</select>
                  </div>
                  <div className="service-line-form-group">
                    <label className="rates-f-label">RATE PERIOD:</label>
                      <select ref="rate_cost_rate_period">{rateperiods.map(option_render)}</select>
                  </div>
                  <div className="service-line-form-group">
                    <label className="rates-f-label">TYPE:</label>
                    <input type="text" className="rates-f-input" ref="rate_cost_type"/>
                  </div>
                  <div className="service-line-form-group">
                    <label className="rates-f-label">STO PPS: </label>
                    <input type="text" className="rates-f-input" ref="rate_cost_pps"/>
                  </div>
                  <div className="service-line-form-group">
                    <label className="rates-f-label">STO SINGLE:</label>
                    <input type="text" className="rates-f-input" ref="rate_cost_single"/>
                  </div>
                  <div className="service-line-form-group">
                    <label className="rates-f-label">THIRD ADULT:</label>
                    <input type="text" className="rates-f-input" ref="rate_cost_third_adult"/>
                  </div>
                  <div className="service-line-form-group">
                    <label className="rates-f-label">CHILD:</label>
                    <input type="text" className="rates-f-input" ref="rate_cost_child"/>
                  </div>
                  <div className="service-line-form-group">
                    <button className="sls-f-submit button">ADD SERVICE LINE RATE</button>
                  </div>
                   </form>
                </div>
                <div className="small-12 medium-6 large-6 columns">
                  <ProductCostList/>
                </div>
               </div>
             );
    }
});
export default connect((state) => {
  return {
    currentSupplier: state.currentSupplier,
    rateperiods: state.rateperiods,
    servicelines: state.serviceLines
  }
})(RateCostsForm);
