var React = require('react');
var ReactDOM = require('react-dom');
//add connect and actions
var {connect} = require('react-redux');
var actions = require('app/actions/actions');
//add list component
import ProductPList from  'ProductPList';

var PolicyForm = React.createClass({
    submitPL: function(e) {
      e.preventDefault();
      var {dispatch, currentSupplier} = this.props;
      //construct post
      var formRef = this.refs;
      var name = formRef.policy_name.value;
      var text  = formRef.policy_text.value;

      if(name.length > 0 && text.length > 0) {
        dispatch(actions.startPolicy(name, text, currentSupplier));
        ReactDOM.findDOMNode(this.refs.form).reset();
      }
    },
    render: function() {
       return(
               <div>
                <div className="row">
                  <div className="small-12 medium-6 large-6 columns">
                    <form onSubmit={this.submitPL} ref="form">
                      <div className="service-line-form-group">
                        <label className="rates-f-label">NAME:</label>
                        <input type="text" className="rates-f-input" ref="policy_name"/>
                      </div>
                      <div className="service-line-form-group">
                        <label className="rates-f-label">POLICY TEXT:</label>
                        <textarea rows="10" className="rates-f-input" ref="policy_text"></textarea>
                      </div>
                      <div className="service-line-form-group">
                        <label className="rates-f-label">IMAGE:</label>
                        <button className="button rates-f-button">Upload File</button>
                      </div>
                      <div className="service-line-form-group">
                        <button className="sls-f-submit button">ADD POLICY</button>
                      </div>
                    </form>
                  </div>
                  <div className="small-12 medium-6 large-6 columns">
                    <ProductPList/>
                  </div>
                </div>
               </div>
             );
    }
});
//export default
export default connect((state) => {
  return {
    currentSupplier: state.currentSupplier
  }
})(PolicyForm)
