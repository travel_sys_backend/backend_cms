var React = require('react');
var {Link} = require('react-router');

var LoginForm = React.createClass({
    handleLogin: function () {
      window.location = '/generic';
    },
    render: function() {
       return(
              <div className="login-component">
                 <div className="form small-12 medium-4 large-4 columns">
                   <p className="login-title">LOGIN TO JETS</p>
                  <form>
                    <label className="text-label">USERNAME</label>
                    <input type="text" ref="username"/>
                    <label className="text-label">PASSWORD</label>
                    <input type="password" ref="password"/>
                    <label className="checkbox-label"><input type="checkbox"/>Remember Me</label>
                    <button className="button login-form-button">LOGIN</button>
                    <div className="signup-link">
                    <Link to="generic" className="s-link">Temp Link to Dashboard</Link>
                    <Link to="signup" className="s-link">SIGN UP</Link>
                    </div>
                  </form>
                 </div>
             </div>
             );
    }
});
module.exports =  LoginForm;
