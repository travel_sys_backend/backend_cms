//we require all the libraries we need
import React,{Component} from 'react'
import {Link, IndexLink} from 'react-router'
import {connect} from 'react-redux'
/*lets build a component
components only require a render method
but you can add more as you wish
*/
export class NavigationBar extends Component {
  render (){
    return(
      <div className="top-bar">
        <div className="top-bar-left">
          <div className="menu">
            <a className="th" href="/">
            <img src={require('../images/jets_logo_white.png')} className="nav-logo"/>
            </a>
          </div>
        </div>
        <div className="top-bar-right">
          <ul className="menu">
            <li>
              <Link to="/">PRODUCTS</Link>
            </li>
            <li>
                <Link to="/">CUSTOMERS</Link>
            </li>
            <li>
                <Link to="/">COMPANY</Link>
            </li>
            <li>
              <Link to="login" className="button">LOGIN</Link>
            </li>
          </ul>
        </div>
      </div>
    );
  }
}
/*Finally we finish by
exporting our class so it can relate to other classes.
Note We are using connect from 'react-redux' to provide
state to our component
*/
export default connect((state) => {
  return state;
}) (NavigationBar);
