var React = require('react');
//components
var InnerNavigationBar = require('InnerNavigationBar');
var LandingBoard = require('LandingBoard');
var BottomBar = require('BottomBar');

var GenericDashboard = React.createClass({
    render: function() {
       return(
               <div>
                 <div className="row">
                   <InnerNavigationBar/>
                   <LandingBoard/>
                  </div>
                  <div className="row bottom-bar-row">
                    <BottomBar/>
                  </div>
               </div>
             );
    }
});
module.exports =  GenericDashboard;
