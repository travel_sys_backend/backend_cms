var React = require('react');
//components
var NavigationBar = require('NavigationBar');
var SignUpForm = require('SignUpForm');
var BottomBar = require('BottomBar');


var SignUpPage = React.createClass({
    render: function() {
       return(
               <div className="signup-page">
                <NavigationBar/>
                <SignUpForm/>
                  <div className="row bottom-bar-row">
                    <BottomBar/>
                  </div>
               </div>
             );
    }
});
module.exports =  SignUpPage;
