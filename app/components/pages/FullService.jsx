var React = require('react');
//3rd party
var {Link} = require('react-router');
 //componests
var InnerNavigationBar = require('InnerNavigationBar');
var BottomBar = require('BottomBar');

var FullService = React.createClass({
    render: function() {
       return(
               <div>
                <div clasName="row">
                  <InnerNavigationBar/>
                    <div className="row">
                     <div className="small-12 medium-3 large-3 columns">
                       <div className="upper-s-sidebar row">
                         <div>
                           <p className="sidebar-contact-title"> Contact Details </p>
                         </div>
                         <div>
                           <p className="sidebar-contact-info">res@mannabay.com</p>
                           <p className="sidebar-contact-info">+27 21 461 89 15</p>
                           <p className="sidebar-contact-info">gm@mannabay.com</p>
                         </div>
                         <div className="sidebar-contact-buttons">
                           <Link to="/serviceitem/slinfo" className="bt-info button">Information</Link>
                           <Link to="/serviceitem/slrates" className="bt-info button">Services & Rates</Link>
                           <Link to="/serviceitem/slpols" className="bt-info button">Policies</Link>
                         </div>
                       </div>
                       <div className="lower-s-sidebar row">
                         <form>
                           <div>
                             <input type="text" placeholder="Client Name"/>
                           </div>
                           <div className="fs-in-cont">
                             <input className="in-next-a" type="text" placeholder="Arrival"/>
                             <input className="in-next-b" type="text" placeholder="Depature"/>
                           </div>
                           <div className="select-container">
                             <label className="sidebar-select-label">No. of Travellers
                               <select className="sidebar-select">
                               </select>
                             </label>
                           </div>
                           <div className="select-container">
                             <label className="sidebar-select-label">No. of Rooms
                               <select className="sidebar-select">
                               </select>
                             </label>
                           </div>
                           <div>
                             <textarea rown="4"></textarea>
                           </div>
                           <div className="fs-sb-bt-area">
                             <button className="button fs-sb-button">BOOK NOW</button>
                           </div>
                         </form>
                       </div>
                     </div>
                     <div className="small-12 medium-9 large-9 columns">
                       {this.props.children}
                     </div>
                    </div>
                </div>
                <div className="row bottom-bar-row">
                  <BottomBar/>
                </div>
               </div>
             );
    }
});
module.exports =  FullService;
