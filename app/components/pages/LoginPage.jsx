var React = require('react');

//modules
var NavigationBar = require('NavigationBar');
var LoginForm = require('LoginForm');
var BottomBar = require('BottomBar');

var LoginPage = React.createClass({
    render: function() {
       return(
               <div>
                <div className="row login-header">
                  <NavigationBar/>
                  <LoginForm/>
                </div>
                <div className="row bottom-bar-row">
                  <BottomBar/>
                </div>
               </div>
             );
    }
});
module.exports =  LoginPage;
