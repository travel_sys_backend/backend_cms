var React = require('react');
//components needed for this page
var BottomBar = require('BottomBar');
var InnerNavigationBar = require('InnerNavigationBar');

var Product = React.createClass({
    render: function() {
       return(
              <div>
                <div className="row">
                  <InnerNavigationBar/>
                  {this.props.children}
                </div>
                <div className="row bottom-bar-row">
                  <BottomBar/>
                </div>
              </div>
             );
    }
});
module.exports =  Product;
