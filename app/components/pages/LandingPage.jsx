import React, {Component} from 'react'
import NavigationBar from 'NavigationBar'
var LandingHeader = require('LandingHeader');
var Features = require('Features');
var MainFeaturesSection = require('MainFeaturesSection');
var AboutSection = require('AboutSection');
var BottomBar = require('BottomBar');



export class LandingPage extends Component {
  render() {
    return(
      <div>
        <div className="row header-row">
            <NavigationBar/>
            <LandingHeader/>
        </div>
        <div className="row">
          <Features/>
        </div>
        <div className="features row">
          <MainFeaturesSection/>
        </div>
        <div className="row about">
          <AboutSection/>
        </div>
        <div className="row bottom-bar-row">
          <BottomBar/>
        </div>
      </div>
    );
  }
};
export default LandingPage;
