var React = require('react');

//components
var InnerNavigationBar = require('InnerNavigationBar');
var SpecialsPane = require('SpecialsPane');
var BottomBar = require('BottomBar');

var Specials = React.createClass({
    render: function() {
       return(
               <div>
                 <div className="row">
                  <InnerNavigationBar/>
                  <SpecialsPane/>
                </div>
                <div className="row bottom-bar-row">
                  <BottomBar/>
                </div>
               </div>
             );
    }
});
module.exports =  Specials;
