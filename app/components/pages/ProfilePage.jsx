var React = require('react');
//components
var ProfilePane = require('ProfilePane');
var InnerNavigationBar = require('InnerNavigationBar');
var BottomBar = require('BottomBar');


var ProfilePage = React.createClass({
    render: function() {
       return(
               <div>
                 <div className="row">
                   <InnerNavigationBar/>
                   <ProfilePane/>
                 </div>
                 <div className="row bottom-bar-row">
                   <BottomBar/>
                 </div>
               </div>
             );
    }
});
module.exports =  ProfilePage;
