var React = require('react');

//components
var BottomBar = require('BottomBar');
var InnerNavigationBar = require('InnerNavigationBar');
var ServicesPane = require('ServicesPane');

var Services = React.createClass({
    render: function() {
       return(
               <div>
                 <div className="row">
                   <InnerNavigationBar/>
                   <ServicesPane/>
                 </div>
                 <div className="row bottom-bar-row">
                   <BottomBar/>
                 </div>
               </div>
             );
    }
});
module.exports =  Services;
