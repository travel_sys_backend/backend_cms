var React = require('react');
var NavigationBar = require('NavigationBar');

var Jets = (props) => {
  return (
    <div>
        <div className="app">
          {/*render children here*/}
          {props.children}
        </div>
    </div>
  );
}

module.exports = Jets;
