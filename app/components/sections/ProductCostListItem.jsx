var React = require('react');
//add connect and export
var {connect} = require('react-redux');
var actions = require('app/actions/actions');

var ProductCostListItem = React.createClass({
    render: function() {
      var {rate_period, service_line, type, single, pps, thirdadu, id, dispatch, child} = this.props
       return(
               <div>
                <p>{rate_period}</p>
                <p> </p>
                  <div className="cost-list row">
                    <div className="sto-column small-2 medium-2 large-2 columns">
                      <p>STO PPS</p>
                      <p>{pps}</p>
                    </div>
                    <div className="rate-list-col small-2 medium-2 large-2 columns text-center">
                     <p>STO SINGLE</p>
                     <p>{single}</p>
                    </div>
                    <div className="rate-list-col small-2 medium-2 large-2 columns text-center">
                      <p>3RD ADULT</p>
                      <p>{thirdadu}</p>
                    </div>
                    <div className="rate-list-col small-2 medium-2 large-2 columns text-center">
                      <p>CHILD</p>
                      <p>{child}</p>
                    </div>
                  </div>
               </div>
             );
    }
});
export default connect()(ProductCostListItem);
