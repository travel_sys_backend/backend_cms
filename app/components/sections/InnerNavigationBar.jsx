var React = require('react');
//imports
var {Link} = require('react-router');

var InnerNavigationBar = React.createClass({
    render: function() {
       return(
               <div>
                 <div className="top-bar top-bar-inner">
                   <div className="top-bar-left">
                     <div className="menu menu-inner">
                       <a className="th" href="/">
                       <img src={require('../../images/logo_jets_colour.png')} className="nav-logo"/>
                       </a>
                     </div>
                   </div>
                   <div className="top-bar-right">
                     <ul className="menu menu-inner">
                       <li>
                         <Link to="profile">YOUR PROFILE</Link>
                       </li>
                       <li>
                           <Link to="specials">SPECIALS</Link>
                       </li>
                       <li>
                           <Link to="services">SERVICES</Link>
                       </li>
                       <li>
                         <Link to="login" className="button">LOGOUT</Link>
                       </li>
                     </ul>
                   </div>
                 </div>
               </div>
             );
    }
});
module.exports =  InnerNavigationBar;
