var React = require('react');

var PasswordChangeSuccessModal = React.createClass({
  componentDidMount: function () {
    var modal = new Foundation.Reveal($('#password-change-success'));
    modal.open();
    },
    render: function() {
       return(
               <div id="password-change-success" className="reveal small text-center" data-reveal>
                 <p className="success-message">YOUR PASSWORD WAS SUCCESSFULLY CHANGED</p>
                 <img src={require('../../images/ic_complete.png')} className="success-icon"/>
               </div>
             );
    }
});
module.exports =  PasswordChangeSuccessModal;
