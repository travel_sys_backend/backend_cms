var React = require('react');

//components needed in this section
var ProductLinks = require('ProductLinks');
import PropertyInfo from 'PropertyInfo';
import ServiceLineForm from 'ServiceLineForm';
import RatePeriodsForm from 'RatePeriodsForm';
import PolicyForm from 'PolicyForm';
import RateCostsForm from 'RateCostsForm';

var AddRates = React.createClass({
    render: function() {
       return(
               <div className="row">
                <div className="small-12 medium-45 large-3 columns">
                  <ProductLinks/>
                </div>
                <div className="small-12 medium-8 large-9 columns">
                  <div className="row">
                    <p className="rate-section-title"> 1. Supplier Details</p>
                    <PropertyInfo/>
                  </div>
                  <div className="row">
                    <p className="rate-section-title"> 2. Add Service Line</p>
                    <ServiceLineForm/>
                  </div>
                  <div className="row">
                    <p className="rate-section-title"> 3. Add Policy </p>
                    <PolicyForm/>
                  </div>
                  <div className="row">
                    <p className="rate-section-title"> 4. Set Rate Periods </p>
                    <RatePeriodsForm/>
                  </div>
                  <div className="row">
                    <p className="rate-section-title"> 5. Add Rates</p>
                    <RateCostsForm/>
                  </div>
                </div>
               </div>
             );
    }
});
module.exports =  AddRates;
