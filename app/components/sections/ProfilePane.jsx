var React = require('react');
//components needed
var ChangePasswordModal = require ('ChangePasswordModal');

var ProfilePane = React.createClass({
    handleChangePassword: function (buttonEvent) {
      buttonEvent.preventDefault();
      this.setState({
        showChangeModal: true
      })
    },
    getInitialState: function () {
      return {
        showChangeModal: false
      }
    },
    render: function() {
      var {showChangeModal} = this.state;
      function renderChangePwd() {
        if(showChangeModal) {
          return (
            <ChangePasswordModal/>
          );
        }
      }

       return(
              <div className="profile-pane">
                  <div className="profile-sidebar small-12 medium-2 large-2 columns">
                  </div>
                  <div className="profile-form small-12 medium-10 large-10 columns">
                    <div className="row">
                      <p className="details-title"> Your Details </p>
                    </div>
                    <div className="row">
                      <div className="small-12 large-8 medium-8  columns">
                        <form>
                          <div className="form-group">
                            <label className="blue-label">Username:</label>
                            <input type="text" ref="profusername" placeholder="mcron24"/>
                          </div>
                          <div className="form-group">
                            <label className="blue-label">Name:</label>
                            <input type="text" ref="profname" placeholder="Marike Cronje"/>
                          </div>
                          <div className="form-group">
                            <label className="blue-label">Email:</label>
                            <input type="text" ref="profemail" placeholder="marike@ker-downeyafrica.com"/>
                          </div>
                          <div className="form-group">
                            <label className="blue-label">Phone:</label>
                            <input type="text" ref="profphone" placeholder="+27 (0) 85 465 2218"/>
                          </div>
                          <div className="form-group">
                            <label className="blue-label">Field:</label>
                            <input type="text" ref="proffield" placeholder="Marketing"/>
                          </div>
                          <div className="form-group">
                            <label className="blue-label">Company:</label>
                            <input type="text" ref="profcompany" placeholder="Company"/>
                          </div>
                          <div className="button-group">
                            <button className="button update-bt">UPDATE INFORMATION</button>
                            <button className="button change-pwd" onClick={this.handleChangePassword}>CHANGE PASSWORD</button>
                          </div>
                        </form>
                      </div>
                      <div className="pic-pane small-12 large-4 medium-4 columns">
                        <p className="prof-pic-label">Profile Picture </p>
                        <img className="prof-image" src={require('../../images/profile.jpg')}/>
                        <button  className="button prof-image-button"> UPLOAD</button>
                      </div>
                    </div>
                  </div>
                  {renderChangePwd()}
              </div>
             );
    }
});
module.exports =  ProfilePane;
