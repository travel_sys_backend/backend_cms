var React = require('react');
var PropTypes = React.PropTypes;

var PolicyList = require('PolicyList');

var ServicePolicies = React.createClass({

  render: function() {
    var policies =  [
      {
        id: 1,
        pname: 'Child Policy',
        image: '../../images/image_child_policy.jpg',
        description: 'A luxury boutique hotel situated at the foot of Table Mountain featuring  8 uniquely decorated suites. You cant get much closer to table Mountain than this',
        policyrates: [
          {
            id: 1,
            desc: '0-2yrs',
            rate: 'free'
          },
          {
            id:2,
            desc: '2-5yrs',
            rate: '50%'
          },
          {
            id:3,
            desc: '5+ yrs',
            rate: '100%'
          }
        ]
      },
      {
        id: 2,
        image: '../../images/image_3rd_adult_policy.jpg',
        pname: '3rd Adult',
        description: 'A luxury boutique hotel situated at the foot of Table Mountain featuring  8 uniquely decorated suites. You cant get much closer to table Mountain than this',
        policyrates: [
          {
            id: 1,
            desc: 'Jan-Mar',
            sto: 300,
            rack: 400
          },
          {
            id: 2,
            desc: 'Apr-Jun',
            sto: 300,
            rack: 400
          },
          {
            id: 3,
            desc: 'Jul-Oct',
            sto: 300,
            rack: 400
          },
          {
            id: 3,
            desc: 'Nov-Dec',
            sto: 300,
            rack: 400
          }
        ]
      }
    ]
    return (
      <div>
        <div>
          <div>
            <p className="slinfo-label">
              Accomodation | Cape Town | Mannabay
            </p>
          </div>
          <div>
            <p className="slinfo-title">
              Mannabay Boutique Hotel
            </p>
          </div>
        </div>
        <div>
          <p className="slinfo-sub-title">
              Services & Rates
          </p>
        </div>
        <div className="slinfo-negotiated">
          <p className="neg-text">30% Negotiated Commission
          </p>
        </div>
        <div className="row">
          <PolicyList policies={policies}/>
        </div>
      </div>
    );
  }

});

module.exports = ServicePolicies;
