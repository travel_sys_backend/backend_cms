var React = require('react');
//components
var BookModal = require('BookModal');

var SpecialItem = React.createClass({
    getInitialState: function() {
      return {
        showBookModal: false
      }
    },
    handleDisplayBooking: function(e) {
      e.preventDefault();
      this.setState({
        showBookModal: true
      });
    },
    render: function() {
      var {showBookModal} = this.state;
      var {type, id, special, valid, imglnk} = this.props;
      function renderBook() {
        if(showBookModal) {
          return(
            <BookModal/>
          );
        }
      }


       return(
               <div className="row">
                 <div className="small-3 medium-3 large-3 columns">
                   <img className="special-pic" src={require('../../images/service_saa.jpg')}/>
                 </div>
                 <div className="special-txt small-9 medium-9 large-9 columns">
                   <div>
                     <span className="special-label">Accomodation:</span> <span className="special-content">Babylonstoren</span>
                   </div>
                   <div>
                    <span className="special-label">Special:</span> <span className="special-content">Free Transfer from Capetown International to Hotel</span>
                   </div>
                   <div>
                     <span className="special-label">Valid:</span> <span className="special-content">21 Sept - 20 Nov 2016</span>
                    </div>
                   <button className="button special-item-button" onClick={this.handleDisplayBooking}>BOOK NOW</button>
                 </div>
                 {renderBook()}
               </div>
             );
    }
});
module.exports =  SpecialItem;
