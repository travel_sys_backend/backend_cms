var React = require('react');

//components we need
var RatesItemList = require('RatesItemList');

var RatesCategory = React.createClass({
    render: function() {
      var {categoryName, rateItems} = this.props;
       return(
               <div>
                <div className="row info-row">
                  <p className="category-row-title">{categoryName}:</p>
                  <RatesItemList rateItems={rateItems}/>
                </div>
               </div>
             );
    }
});
module.exports =  RatesCategory;
