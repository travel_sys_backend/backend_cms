var React = require('react');

//components needed
var RatesItem = require('RatesItem');

var RatesItemList = React.createClass({
    render: function() {
      //pass in rateImes
      var {rateItems}  = this.props;

      /*Render item list if avail*/
      var renderRateItems = () => {
        if(rateItems.length === 0) {
          return (
            <p className="rate-list-message"> No Rate Items to show </p>
          );
        }
        return rateItems.map((item) => {
            return(
              <RatesItem key={item.id} {...item}/>
            );
        })
      };
       return(
               <div>
                 {renderRateItems()}
               </div>
             );
    }
});
module.exports =  RatesItemList;
