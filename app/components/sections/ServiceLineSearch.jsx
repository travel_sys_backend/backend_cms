var React = require('react');

var ServiceLineSearch = React.createClass({
    render: function() {
       return(
               <div>
                 <div className="search-service row">
                   <form className="search-service-form">
                      <input className="flex-form-i"/>
                      <button className="button flex-form-b">SEARCH</button>
                      <select className="flex-form-s">
                        <option>A-Z</option>
                      </select>
                   </form>
                 </div>
               </div>
             );
    }
});
module.exports =  ServiceLineSearch;
