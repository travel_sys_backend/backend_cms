var React = require('react');

var MainFeaturesSection = React.createClass({
    render: function() {
       return(
               <div className="main-features">
                  <div className="feature large-4 columns">
                    <p>BUILD ITINERARIES</p>
                    <img src={require('../../images/ic_build_itinerary.png')}/>
                  </div>
                  <div className="feature large-4 columns">
                    <p>CHECK LIVE AVAILABILITY</p>
                    <img src={require('../../images/ic_live_availability.png')}/>
                  </div>
                  <div className="feature large-4 columns">
                    <p>BOOK ONLINE</p>
                    <img src={require('../../images/ic_book_online.png')}/>
                  </div>
               </div>
             );
    }
});
module.exports =  MainFeaturesSection;
