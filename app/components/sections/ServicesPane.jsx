var React = require('react');
//componests
import SupplierList from 'SupplierList';

var ServicesPane = React.createClass({
    render: function() {
      var services =  [{
        id: 1,
        name: 'name',
        type: 'wohhoo'
      }];
       return(
               <div className="row">
                 <div className="services-sidebar small-12 medium-3 large-3 columns">
                   <form className="services-form">
                     <div className="services-form-group">
                       <label className="services-form-label">Search Services</label>
                       <input type="text"/>
                     </div>
                     <div className="services-form-group">
                       <label className="services-form-label">Country</label>
                       <input type="text"/>
                     </div>
                     <div className="services-form-group">
                       <div className="small-4 medium-4 larger-4 columns">
                         <div>
                           <label className="services-check-label"><input type="checkbox"/>Property</label>
                           <label className="services-check-label"><input type="checkbox"/>Activity</label>
                         </div>
                       </div>
                       <div className="small-4 medium-4 larger-4 columns">
                         <div>
                           <label className="services-check-label"><input type="checkbox"/>Flight</label>
                           <label className="services-check-label"><input type="checkbox"/>Latest</label>
                         </div>
                       </div>
                       <div className="small-4 medium-4 larger-4 columns">
                         <div>
                           <label className="services-check-label"><input type="checkbox"/>Transfers</label>
                         </div>
                       </div>
                     </div>
                     <div className="services-button-area">
                       <button className="button services-form-button">SEARCH</button>
                     </div>
                   </form>
                 </div>
                 <div className="services-content small-12 medium-9 large-9 columns">
                   <SupplierList/>
                 </div>
               </div>
             );
    }
});
module.exports =  ServicesPane;
