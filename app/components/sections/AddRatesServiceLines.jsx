/*Service Line List on the Product Page*/
var React = require('react');
//add connect for fetching state
var {connect} = require('react-redux');
import ProductSLItem from 'ProductSLItem';
var JetsAPI = require('app/api/JetsAPI');


var AddRatesServiceLines = React.createClass({
    render: function() {
      /*get service lines from state*/
      var{serviceLines, currentSupplier} = this.props;
      /*conditional render for  service lines*/
      var renderSLs = () => {
        var currentLines = JetsAPI.currentSet(serviceLines, currentSupplier);
        if(currentLines.length === 0) {
          return(
            <p className="sl_error_message"> No Services Yet</p>
          );
        }
        return serviceLines.map((sl) => {
          return(
            <ProductSLItem key={sl.id} {...sl}/>
          );
        })
      }
       return(
               <div>
                {renderSLs()}
               </div>
             );
    }
});
export default connect((state) => {
  return {
    serviceLines: state.serviceLines,
    currentSupplier: state.currentSupplier
  }
})(AddRatesServiceLines);
