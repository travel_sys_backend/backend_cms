var React = require('react');

var PolicyItem = React.createClass({
    render: function() {
      var {id, pname, description, image} =  this.props;
       return(
               <div className="row policy-item">
                  <div className="text-center small-12 medium-3 large-3 columns">
                    <p className="pname">{pname}</p>
                    <img className="policy-image" src={image}/>
                  </div>
                  <div className="small-12 medium-9 large-9 columns">
                    <p className="policy-desc-p"><span className="policy-blue">Policy Description: </span><span className="policy-t-p">{description}</span></p>
                  </div>
               </div>
             );
    }
});
module.exports =  PolicyItem;
