var React = require('react');

var ItinerarySecondary = React.createClass({
    render: function() {
       return(
               <div className="row">
                <div className="small-12 medium-4 large-3 columns">
                  SideBar
                </div>
                <div className="small-12 medium-8 large-9 columns">
                  Content
                </div>
               </div>
             );
    }
});
module.exports =  ItinerarySecondary;
