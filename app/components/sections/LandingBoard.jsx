var React = require('react');

var LandingBoard = React.createClass({
    render: function() {
       return(
               <div className="landing-board">
                 <div className="sidebar small-12 medium-2 large-2 columns">
                   <p></p>
                 </div>
                 <div className="content small-12 medium-10 large-10 columns">
                   <div className="row">
                     <p className="greeting"> <span className="bold">Hello Jetsetter!</span> How can we help ? </p>
                   </div>
                   <div className="row">
                     <div className="board-section small-12 medium-3 large-3 columns">
                       <img src={require('../../images/consultants_generic_dashboard.jpg')} className="board-pic"/>
                       <p className="board-label con-label">CONSULTING</p>
                       <ul className="con-label">
                         <li>Build an Itinerary</li>
                         <li>Manage Opportunities</li>
                         <li>Send Quotations</li>
                         <li>Check Availability</li>
                       </ul>
                     </div>
                     <div className="board-section small-12 medium-3 large-3 columns">
                       <img src={require('../../images/product_generic_dashboard.jpg')} className="board-pic"/>
                       <p className="board-label prod-label">PRODUCT</p>
                       <ul className="prod-label">
                         <li>Load Rates</li>
                         <li>Create Experiences</li>
                         <li>Manage Service Lines</li>
                       </ul>
                     </div>
                     <div className="board-section small-12 medium-3 large-3 columns">
                       <img src={require('../../images/finance_generic_dashboard.jpg')} className="board-pic"/>
                       <p className="board-label fin-label">FINANCE</p>
                       <ul className="fin-label">
                         <li>Manage Creditor Payments</li>
                         <li>Send Quotes</li>
                         <li>Send Invoices</li>
                         <li>See Transaction History</li>
                       </ul>
                     </div>
                     <div className="board-section small-12 medium-3 large-3 columns">
                       <img src={require('../../images/admin_generic_dashboard.jpg')} className="board-pic"/>
                       <p className="board-label admin-label">ADMIN</p>
                       <ul className="admin-label">
                         <li>Analytics & Reports</li>
                         <li>Targetss</li>
                       </ul>
                     </div>
                   </div>
                   <div className="row buttons-row">
                      <div className="board-section small-12 medium-3 large-3 columns">
                         <button className="button con-button">START</button>
                      </div>
                      <div className="board-section small-12 medium-3 large-3 columns">
                         <button className="button prod-button">START</button>
                      </div>
                      <div className="board-section small-12 medium-3 large-3 columns">
                         <button className="button fin-button">START</button>
                      </div>
                      <div className="board-section small-12 medium-3 large-3 columns">
                         <button className="button admin-button">START</button>
                      </div>
                   </div>
                 </div>
               </div>
             );
    }
});
module.exports =  LandingBoard;
