var React = require('react');
//components needed in this section
var ProductLinks = require('ProductLinks');

var ItineraryPrimary = React.createClass({
    render: function() {
       return(
             <div className="row">
              <div className="small-12 medium-4 large-3 columns">
               <ProductLinks/>
              </div>
              <div className="small-12 medium-8 large-9 columns">
                Content
              </div>
             </div>
             );
    }
});
module.exports =  ItineraryPrimary;
