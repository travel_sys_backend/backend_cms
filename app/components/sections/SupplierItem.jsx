/*Supplier List Item*/
var React = require('react');
//collect connect and actions to use in transactions
var {connect} = require('react-redux');
var {Link} = require('react-router');
var actions = require('app/actions/actions');

var SupplierItem = React.createClass({
    render: function() {
      /*get data from state for rendering*/
      var {category, name,fact_sheet, id} = this.props;
        return(
               <div className="row">
                 <div className="small-3 medium-3 large-3 columns">
                   <img className="service-list-image" src={require('../../images/service_samsara.jpg')}/>
                 </div>
                 <div className="sl-content small-9 medium-9 large-9 columns">
                   <div className="service-item-group">
                     <span className="service-item-label">{category}:</span><span className="service-item-content">{name}</span>
                   </div>
                   <div className="service-item-group">
                     <span className="service-item-label">Service:</span><span className="service-item-content">{fact_sheet}</span>
                   </div>
                   <div className="service-list-button-area">
                     <button className="button service-info-button">MORE INFORMATION</button>
                   </div>
                   <Link to="serviceitem" className="s-link">Temp Link to Service Page</Link>
                 </div>
               </div>
             );
    }
});
export default connect()(SupplierItem)
