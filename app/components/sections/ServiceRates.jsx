var React = require('react');

//component
var RatesList = require('RatesList');

var ServiceRates = React.createClass({
    getInitialState: function() {
        return {
          rateCategories: [
                    {
                      id: 1,
                      categoryName: 'Room Types',
                      rateItems: [
                        {
                          id: 1,
                          rateImg: '../../images/image_room_type_standard.jpg',
                          rateItemName: 'Standard Suite',
                          rateFromCost: 'From $200 /person'

                        },
                        {
                          id: 2,
                          rateImg: '../../images/image_room_type_deluxe.jpg',
                          rateItemName: 'Deluxe Suite',
                          rateFromCost: 'From $250/person'
                        },
                        {
                          id: 3,
                          rateImg: '../../images/image_room_type_mountain_suite.jpg',
                          rateItemName: 'Mountain Suite',
                          rateFromCost: 'From $220/person'
                        }
                    ]
                    },
                    {
                      id: 2,
                      categoryName: 'Transfers',
                      rateItems: [
                        {
                          id: 1,
                          rateImg: '../../images/image_transfer.jpg',
                          rateItemName: 'Luxury Vehicle',
                          rateFromCost: 'From $50/person'
                        },
                      ]
                    }
                  ]
                };
              },
          render : function() {
            var {rateCategories} = this.state;
            return (
                    <div>
                      <div>
                        <div>
                          <p className="slinfo-label">
                            Accomodation | Cape Town | Mannabay
                          </p>
                        </div>
                        <div>
                          <p className="slinfo-title">
                            Mannabay Boutique Hotel
                          </p>
                        </div>
                      </div>
                    <div>
                        <p className="slinfo-sub-title">
                            Services & Rates</p>
                    </div>
                    <div className="slinfo-negotiated">
                      <p className="neg-text">30% Negotiated Commission
                      </p>
                    </div>
                    <div className="row">
                      <RatesList rateCategories={rateCategories}/>
                    </div>
                  </div>
                        );
                        }
});
module.exports = ServiceRates;
