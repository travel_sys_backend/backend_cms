var React = require('react');

var ServiceLineInfo = React.createClass({
    render: function() {
       return(
               <div className="row info-row">
                <div>
                  <p className="slinfo-label"> Accomodation | Cape Town | Mannabay </p>
                </div>
                <div>
                  <p className="slinfo-title"> Mannabay Boutique Hotel</p>
                </div>
                <div>
                  <p className="slinfo-sub-title"> Mannabay Boutique Hotel</p>
                 </div>
                 <div className="slinfo-negotiated">
                   <p className="neg-text">30% Negotiated Commission</p>
                 </div>
                 <div className="row">
                   <div className="small-3 medium-3 large-3 columns">
                     <img className="slinfo-image" src={require('../../images/image_service_line_mannabay.jpg')}/>
                   </div>
                   <div className="small-9 medium-9 large-9 columns">
                     <p><span className="sl-info-title">Location:</span><span className="sl-info-content"> 8 Bridle Road, Oranjezicht, Cape Town, South Africa </span></p>
                     <p><span className="sl-info-title">Accommodation Type:</span><span className="sl-info-content">5-Star Boutique Hotel</span></p>
                     <p><span className="sl-info-title">Description: </span><span className="sl-info-content">A luxury boutique hotel situated on the foot of Table Mountain,
                       featuring 8 uniquely decorated suites. You can’t get much closer to Table Mountain than this… A
                       truly extraordinary luxury boutique hotel in the heart of CapeTown,
                       Manna Bay is a gateway to the
                       table-topped landmark and its natural mystique. Quite literally. A gate leads directly from MannaBay’s
                       grounds to the Table Mountain National Park.</span></p>
                     <p><span className="sl-info-title">Number of Rooms:  </span><span className="sl-info-content"> 8 Rooms</span></p>
                     <p><span className="sl-info-title">Facilities:</span><span className="sl-info-content">Swimming Pool, Gym, Spa, Library, Boma, Bar</span></p>
                     <p><span className="sl-info-title">Services: </span><span className="sl-info-content">Butler Service, Transfers, Laundry, Mobile Phones, WiFi</span></p>
                     <p><span className="sl-info-title">Child-Friendly:  </span><span className="sl-info-content">Yes.</span></p>
                     <p><span className="sl-info-title">Wheelchair Access:</span><span className="sl-info-content"> Yes.</span></p>
                   </div>
                 </div>

               </div>
             );
    }
});
module.exports =  ServiceLineInfo;
