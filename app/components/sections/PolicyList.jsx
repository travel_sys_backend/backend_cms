var React = require('react');
var PolicyItem = require('PolicyItem');

var PolicyList = React.createClass({
    render: function() {
      //get the list of policies from state
      var {policies} = this.props;

      //render policies if exist
      var renderPolicies = () => {
        return policies.map((policy) => {
          return(
          <PolicyItem key={policy.id} {...policy}/>
          );
        })
      };
       return(
               <div className="policy-list">
                {renderPolicies()}
               </div>
             );
    }
});
module.exports =  PolicyList;
