var React = require('react');
//add connect to fetch state and item component
var {connect} = require('react-redux');
import ProductPItem from 'ProductPItem';
var JetsAPI = require('app/api/JetsAPI');


var ProductPList = React.createClass({
    render: function() {
      //get policies from state
      var {policies, currentSupplier} = this.props;
      /*conditional render of policies  else show error*/
      var renderPolicies = () => {
        var currentPolicies = JetsAPI.currentSet(policies, currentSupplier);
        if(currentPolicies.length === 0) {
          return (
            <p className="empty_error"> No Policies Yet</p>
          );
        }

        return policies.map((policy) => {
          return(
            <ProductPItem key={policy.id} {...policy}/>
          );
        });
      }
       return(
               <div>
                {renderPolicies()}
               </div>
             );
    }
});
export default connect((state) => {
  return {
    policies: state.policies,
    currentSupplier: state.currentSupplier
  }
})(ProductPList)
