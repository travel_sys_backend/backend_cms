var React = require('react');
//add connect to fetch periods from state
var {connect} = require('react-redux');
import ProductPeriodItem from 'ProductPeriodItem';
var JetsAPI = require('app/api/JetsAPI');

var ProductPeriodList = React.createClass({
    render: function() {
      //get vars from state
      var {rateperiods, currentSupplier} = this.props;
      /*conditinally render periods*/
      var renderPeriods = () => {
        var currentPeriods = JetsAPI.currentSet(rateperiods, currentSupplier);
        if(currentPeriods.length === 0 ) {
          return(
            <p className="empty_error">No Rate Periods Yet</p>
          );
        }
        return rateperiods.map((rp) => {
          return(
            <ProductPeriodItem key={rp.id} {...rp}/>
          );
        })
      }
       return(
               <div>
                {renderPeriods()}
               </div>
             );
    }
});
export default connect((state) => {
  return {
    rateperiods: state.rateperiods,
    currentSupplier: state.currentSupplier
  }
})(ProductPeriodList);
