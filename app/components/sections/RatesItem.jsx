var React = require('react');

//components needed
//var RatesItemCostList = require('RatesItemCostList')

var RatesItem = React.createClass({
  render: function() {
    var {rateImg, rateItemName, rateFromCost} = this.props;
       return(
               <div>
                 <div className="row rate-item">
                  <div className="rates-image small-3 medium-3 large-3 columns">
                    <img src={rateImg} className="rates-item-image"/>
                    <p className="rate-item-name text-center">{rateItemName}</p>
                    <p className="rates-item-cost text-center">{rateFromCost}</p>
                  </div>
                  <div className="rates-costs small-12 medium-9 large-9 columns">

                  </div>
                </div>
               </div>
             );
    }
});
module.exports =  RatesItem;
