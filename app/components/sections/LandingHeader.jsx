var React = require('react');

var LandingHeader = React.createClass({
    render: function() {
       return(
               <div className="landing-header">
                <p className="super">TRAVEL EASY TECHNOLOGY</p>
                <p className="sub">SOMETHING FOR EVERYONE</p>
               </div>
             );
    }
});
module.exports =  LandingHeader;
