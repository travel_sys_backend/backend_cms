var React = require('react');

//component
var SpecialItem = require('SpecialItem');

var SpecialsList = React.createClass({
    render: function() {
      /*get the specials passed in */
      var {specials} = this.props;

      /*Check is specials exist in array and render*/
      var renderSpecials = () => {
        if(specials.length === 0){
          return(
            <p className="empty-specials">No specials Currently</p>
          );
        }
        return specials.map((special) => {
          return(
            /*Pass in a special and render it*/
            <SpecialItem key={special.id} {...special}/>
          );
        })
      };
       return(
               <div>
                {renderSpecials()}
               </div>
             );
    }
});
module.exports =  SpecialsList;
