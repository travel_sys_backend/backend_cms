var React = require('react');

var Features = React.createClass({
    render: function() {
       return(
         <div className="features-row">
                <div  className="row">
                  <div className="small-4 large-4 columns">
                    <div className="row">
                      <div className="list-logo small-2 medium-2 large-2 columns">
                        <img src={require('../../images/icon_jets_blue.png')}/>
                      </div>
                      <div className="list-list small-10 medium-10 large-10 columns">
                        <p className="feature-title">CONSULTANTS</p>
                        <p className="title-sub">tour operators, travel agents, DMCS</p>
                        <ul className="no-bullet">
                          <li>Build Itineraries</li>
                          <li>Manage Opportunities</li>
                          <li>Search Service Lines</li>
                          <li>Book Online</li>
                          <li>Generate Iteineraries</li>
                        </ul>
                      </div>
                  </div>
                  </div>
                  <div className="small-4 large-4 columns">
                      <div className="list-logo small-2 medium-2 large-2 columns">
                        <img src={require('../../images/icon_jets_blue.png')}/>
                      </div>
                      <div className="list-list small-10 medium-10 large-10 columns">
                        <p className="feature-title">SUPPLIERS</p>
                        <p className="title-sub">accomodation, restaurants, flights</p>
                        <div className="row">
                        <ul className="no-bullet">
                          <li>Load Rates</li>
                          <li>Load Availability</li>
                          <li>Manage Service Lines</li>
                        </ul>
                      </div>
                    </div>
                  </div>

                  <div className="small-4 large-4 columns">

                    <div className="row">
                      <div className="list-logo small-2 medium-2 large-2 columns">
                        <img src={require('../../images/icon_jets_blue.png')}/>
                      </div>
                      <div className="list-list small-10 medium-10 large-10 columns">
                        <p className="feature-title">FINANCE</p>
                        <p className="title-sub">quotations,invoices, payments</p>
                        <ul className="no-bullet">
                          <li>Generate Invoices</li>
                          <li>Search Transactions</li>
                          <li>Creditor Payments</li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              <div className="row feat-bottom">
                <div className="small-4 large-4 columns">
                    <div className="row">
                      <div className="list-logo small-2 medium-2 large-2 columns">
                        <img src={require('../../images/icon_jets_blue.png')}/>
                      </div>
                      <div className="list-list small-10 medium-10 large-10 columns">
                        <p className="feature-title">MARKETING</p>
                        <p className="title-sub">reports, statistics, reach</p>
                        <ul className="no-bullet">
                          <li>View Analytics</li>
                          <li>See Monthly Reports</li>
                          <li>Manage Sales</li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <div className="small-4 large-4 columns admin">
                    <div className="row">
                      <div className="list-logo small-2 medium-2 large-2 columns">
                        <img src={require('../../images/icon_jets_blue.png')}/>
                      </div>
                      <div className="list-list small-10 medium-10 large-10 columns">
                        <p className="feature-title">ADMIN</p>
                        <p className="title-sub"> reports, statistics, reach</p>
                        <ul className="no-bullet">
                          <li>View Analytics</li>
                          <li>See Monthly Reports</li>
                          <li>Manage Sales</li>
                        </ul>
                    </div>
                    </div>
                  </div>
                </div>
               </div>
       );
    }
});
module.exports =  Features;
