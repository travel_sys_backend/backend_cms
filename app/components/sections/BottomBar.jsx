var React = require('react');

var BottomBar = React.createClass({
    render: function() {
       return(
               <div className="bottom-bar">
                 <div className="row">
                   <div className="small-12 medium-2 large-2 columns">
                     <p className="footer-title">PRODUCTS</p>
                     <ul className="footer-list no-bullet">
                       <li>JETS for Operators</li>
                       <li>JETS for Suppliers</li>
                     </ul>
                   </div>
                   <div className="small-12 medium-2 large-2 columns">
                     <p className="footer-title">RATES</p>
                      <ul className="footer-list no-bullet">
                        <li>JETS for Operators</li>
                        <li>JETS for Suppliers</li>
                      </ul>
                   </div>
                   <div className="small-12 medium-2 large-2 columns">
                     <p className="footer-title">LIVE AVAILABILITY</p>
                      <ul className="footer-list no-bullet">
                        <li>JETS for Operators</li>
                        <li>JETS for Suppliers</li>
                      </ul>
                   </div>
                   <div className="small-12 medium-2 large-2 columns">
                     <p className="footer-title">ABOUT US</p>
                      <ul className="footer-list no-bullet">
                        <li>JETS for Operators</li>
                        <li>JETS for Suppliers</li>
                      </ul>
                   </div>
                   <div className="small-12 medium-2 large-2 columns">
                     <p className="footer-title">CONNECT</p>
                      <ul className="footer-list no-bullet">
                        <li>Contact Us </li>
                      </ul>
                   </div>
                 </div>
                 <div className="row">
                   <div className="small-12 large-4 columns">
                     <img className="footer-logo" src={require('../../images/logo_jets_colour.png')}/>
                   </div>
                   <div className="small-12 large-4 columns tos">
                     TERMS OF USE | SITE FEEDBACK
                   </div>
                   <div className="small-12 large-4 columns">

                   </div>
                 </div>
               </div>
             );
    }
});
module.exports =  BottomBar;
