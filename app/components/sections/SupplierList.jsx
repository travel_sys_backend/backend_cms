/*Suppliers List Can be ReUsed in Services Page*/
var React = require('react');
//Add connect and actions
var {connect} = require('react-redux');
import SupplierItem from 'SupplierItem';

var SupplierList = React.createClass({
    render: function() {
      var {suppliers} = this.props;
      /*Check is specials exist in array and render*/
      var renderSuppliers = () => {
        if(suppliers.length === 0){
          return(
            <p className="empty-specials">No Suppliers Currently</p>
          );
        }
        return suppliers.map((supplier) => {
          return(
            /*Pass in a special and render it*/
            <SupplierItem key={supplier.id} {...supplier}/>
          );
        })
      };
       return(
               <div>
                 {renderSuppliers()}
               </div>
             );
    }
});
export default connect((state) => {
  return {
    suppliers: state.suppliers
  }
})(SupplierList);
