var React = require('react');
//components
var RatesCategory = require('RatesCategory');

var RatesList = React.createClass({
    render: function() {
      /*Render list here if data is avail*/
      var {rateCategories} = this.props;

      var renderCategories = () => {
        if(rateCategories.length === 0) {
          return (
             <p className="rate-list-message"> No Rates to show </p>
           );
        }
        return rateCategories.map((category) => {
          return (
            <RatesCategory key={category.categoryName} {...category}/>
          );
        })
      };

       return (
               <div>
                {renderCategories()}
               </div>
             );
    }
});
module.exports =  RatesList;
