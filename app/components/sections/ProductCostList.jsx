var React = require('react');
//add connect and items  components
var {connect} = require('react-redux');
import ProductCostListItem from 'ProductCostListItem';
var JetsAPI = require('app/api/JetsAPI');

var ProductCostList = React.createClass({
    render: function() {
      //pick costs from state
      var {ratecosts, currentSupplier} = this.props;
      /*Conditional Render*/
      var renderCosts = () => {
        var currentCosts = JetsAPI.currentSet(ratecosts, currentSupplier);
        if(currentCosts.length === 0) {
          return(
            <p  className="empty_error"> No Costs Yet</p>
          );
        }
        return ratecosts.map((cost) => {
          return (
            <ProductCostListItem key={cost.id} {...cost}/>
            );
        })
      }
       return(
               <div>
                {renderCosts()}
               </div>
             );
    }
});
export default connect((state) => {
  return {
    ratecosts: state.ratecosts,
    currentSupplier: state.currentSupplier
  }
})(ProductCostList);
