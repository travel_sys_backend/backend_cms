var React = require('react');

//components needed in this section
var ProductLinks = require('ProductLinks');
var BuildItineraryForm = require('BuildItineraryForm');
var ServiceLineSearch = require('ServiceLineSearch');
import SupplierList from 'SupplierList';

var Build = React.createClass({
    render: function() {
       return(
               <div className="row">
                <div className="small-12 medium-4 large-3 columns">
                <ProductLinks/>
                <BuildItineraryForm/>
                </div>
                <div className="small-12 medium-8 large-9 columns">
                  <ServiceLineSearch/>
                  <SupplierList/>
                </div>
               </div>
             );
    }
});
module.exports =  Build;
