var React = require('react');

var BookModal = React.createClass({
  componentDidMount: function() {
    var modal = new Foundation.Reveal($('#book-modal'));
    modal.open();
  },
    render: function() {
       return(
               <div id="book-modal" className="reveal small">
                 <p className="book-modal-title text-center">BOOK SPECIAL</p>
                 <form className="book-modal=form">
                   <div className="modal-form-group">
                     <label className="book-label">TO</label>
                     <input className="book-input" type="text"/>
                   </div>
                   <div className="modal-form-group">
                     <label className="book-label">FROM</label>
                     <input className="book-input" type="text"/>
                   </div>
                   <div className="modal-form-group">
                     <label className="book-label">SUBJECT</label>
                     <input className="book-input" type="text"/>
                   </div>
                   <div className="modal-form-group">
                     <textarea rows="5"></textarea>
                   </div>
                   <div className="book-button-area">
                     <button className="button book-button">SEND EMAIL</button>
                    </div>
                 </form>
               </div>
             );
    }
});
module.exports =  BookModal;
