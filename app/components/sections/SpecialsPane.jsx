var React = require('react');

var SpecialsList = require('SpecialsList');

var SpecialsPane = React.createClass({
    render: function() {
      var specials = [{
        id:1 ,
        special:'special',
        valid: 'valid',
        type: 'accomodation'
      }];
       return(
               <div className="row">
                <div className="small-12 medium-3 large-3 columns">
                  <form className="specials-form">
                    <label className="specials-form-label">Search Specials</label>
                    <input type="text" ref="specialsSearchText"/>
                    <label className="specials-form-label">Date Range</label>
                    <input type="text" ref="specialsStartRange" />
                    <input type="text" ref="specialEndRange"/>
                    <label className="specials-form-label">Country</label>
                    <div className="row specials-form-check">
                      <div className="small-4 medium-4 large-4 columns">
                        <label className="specials-form-label-check"><input type="checkbox" ref=""/>Property</label>
                        <label className="specials-form-label-check"><input type="checkbox" ref=""/>Activity</label>
                      </div>
                      <div className="small-4 medium-4 large-4 columns">
                        <label className="specials-form-label-check"><input type="checkbox" ref=""/>Flight</label>
                        <label className="specials-form-label-check"><input type="checkbox" ref=""/>Latest</label>
                      </div>
                      <div className="small-4 medium-4 large-4 columns">
                        <label className="specials-form-label-check"><input type="checkbox" ref=""/>Transfer</label>
                      </div>
                    </div>
                    <div className="form-button">
                     <button className="button specials-form-button">SEARCH</button>
                    </div>
                  </form>
                </div>
                <div className="small-12 medium-9 large-9 columns">
                  <p className="special-pagination-title">Latest Specials | 1 result | page 1 </p>
                  <SpecialsList specials={specials}/>
                </div>
               </div>
             );
    }
});
module.exports =  SpecialsPane;
