import firebase from 'firebase';

/*
*Use Environment variabes to support automated testing and Development database
*/
//try for error protection
try {
  // Initialize Firebase
  var config = {
    apiKey: 'AIzaSyDy6V9pMISp62OdN0KuXs5wPmxDVpPGxdU',//process.env.API_KEY, //we use this tp access the api
    authDomain: 'jets-backennd.firebaseapp.com', //process.env.AUTH_DOMAIN, //this is the domains allowed to access us
    databaseURL: 'https://jets-backennd.firebaseio.com',//process.env.DATABASE_URL, // this is the path to the database
    storageBucket: 'jets-backennd.appspot.com',//process.env.STORAGE_BUCKET, //this is the path to our space for files
  };
  var PATH = "search";
  firebase.initializeApp(config);
} catch(e) {

}

//get database root reference
export var firebaseRef = firebase.database().ref();
export default firebase;
