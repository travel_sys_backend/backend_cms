//add jquery
import firebase, {firebaseRef} from 'app/firebase/index'

var $ = require('jquery');
//export an object that contains all the functions
module.exports = {
  currentSet: (set, currentSupplier) => {
    return set.filter((item) => {
      return item.supplier == currentSupplier;
    });
  },
  searchDB: (term) => {

    //build a query
    var query = {
      index: 'firebase',
      type: 'supplier'
    }

    // add term
    query.q = term;

    //do the search
    var searchRef = firebaseRef.child('search');
    var key = searchRef.child('request').push(query).key;
    console.log('search', key, query);

    //read results
    searchRef.child('response/'+key).on('value', (snap) => {
      var dat = snap.val();
      if( dat === null ) { return; } // wait until we get data
      console.log('found', dat.hits.length, 'items', dat);
    });

  }
};
