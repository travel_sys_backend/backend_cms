/*Add dependencies for testing actions*/
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import expect from 'expect';
import firebase, {firebaseRef} from 'app/firebase';
import moment from 'moment';
import * as actions from 'app/actions/actions'//TODO read up on import and require difference

/*Lets Make a mock store for testing CRUD Actions*/
var createMockStore = configureMockStore([thunk]); //TODO read  up

/*Assertions for actions*/
describe('Actions', () => {
    //assert that actions exist check if compiling and imports working
    it('should exist',  () => {
      expect(actions).toExist();
    });


    describe('Supplier Actions', () => {
      describe('Current Supplier', () => {
        it('should generate SET_CURR_SUPPLIER action', () => {
          var action = {
            type: 'SET_CURR_SUPPLIER',
            key:  'xkcd234'
          }

          var res = actions.currSupplier(action.key);

          expect(res).toEqual(action);
        });
      });

      describe('Add Supplier',  () => {
        it('should generate ADD_SUPPLIER action', () => {
          var supplier = {
            name: 'new supplier',
            fact_sheet: 'fact sheet',
            liability_sheet: 'liability sheet',
            group: 'group',
            service_lines: null,
            policies: null,
            rate_periods: null,
            rate_costs: null,
            contracts: null,
            cancellation_terms: null,
            deposit_terms: null,
            commission: '22',
            creation_date: moment().unix(),
            country: 'SA',
            region: 'Capetown',
            email: 'mail@m.com',
            telephone: '2797179',
            website: 'ww.ww.ww',
            category: 'cat',
            rating: '100'
          };

          var action = {
            type: 'ADD_SUPPLIER',
            supplier
          };

          var res = actions.addSupplier(action.supplier);

          expect(res).toEqual(action);
        });
      });

      describe('UPDATE SUPPLIER', () => {
        var action = {
          type:'UPDATE_SUPPLIER',
          id: 'xkcd234',
          updates: {
            region: 'Okavango'
          }
        };

        var res = actions.updateSupplier(action.id, action.updates);
        it('should generate UPDATE_SUPPLIER action', () =>{
          expect(res).toEqual(action);
        });
      });

      describe('DELETE SUPPLIER', () => {
        it('should generate DELETE_SUPPLIER action', () => {
          var action = {
            type: 'DELETE_SUPPLIER',
            key: 1
          };

          var res = actions.deleteSupplier(action.key);

          expect(res).toEqual(action);
        });
      });

    });

    describe('Supplier Async Actions', () => {
        var testSupplierRef;

        //sample struct
        var supplier = {
          name: 'new supplier',
          fact_sheet: 'fact sheet',
          liability_sheet: 'liability sheet',
          group: 'group',
          service_lines: null,
          policies: null,
          rate_periods: null,
          rate_costs: null,
          contracts: null,
          cancellation_terms: null,
          deposit_terms: null,
          commission: '22',
          creation_date: moment().unix(),
          country: 'SA',
          region: 'Capetown',
          email: 'mail@m.com',
          telephone: '2797179',
          website: 'ww.ww.ww',
          category: 'cat',
          rating: 100
        };
        /*Impement BeforeEach and After  to add items
        * To firebase and delete them after we have run this tests
        */

        //Before Each this is called before every test case in this block
        //takes a function to run before each of the tests here
        beforeEach((done) => {
          var suppliersRef = firebaseRef.child('suppliers');

          //wipe all suppliers
          suppliersRef.remove().then(() => {
            //make a sample
            testSupplierRef = firebaseRef.child('suppliers').push();

            //load a payload into the new supplier
            return testSupplierRef.set(supplier);
          }).then(() => done())
          .catch(done);
        });


        //Use afterEach to clear db
        afterEach((done) => {
          testSupplierRef.remove().then(() => done());
        });


        it('should create a supplier then dispatch ADD_SUPPLIER', (done) => {
          //mock a store
          const store = createMockStore({});
          //vars for creating a supplier
          const name = 'name';
          const fact_sheet = 'facts';
          const liability_sheet = 'liable';
          const group = 'singita';
          const commission = 22;
          const country = 'South Africa';
          const region ='Cape Town';
          const email = 'mymail@me.com';
          const tel = 8080800;
          const website  = 'w.w.w';
          const category = 'accomodation';
          const rating = 22;

          //dispatch out async action to our mock store
          store.dispatch(actions.startAddSupplier(name, fact_sheet, liability_sheet,
          group, commission, country, region, email,
          tel, website, category, rating)).then(() => {

            //get mocked store actions
            const actions = store.getActions();

            //assertions
            //This is a hack while figuring out why I cant query action
            //TODO query for action not supplier
            expect(actions[0].supplier).toExist();
            done();
          }).catch(done);
        });

        it('should update a supplier and dispatch UPDATE_SUPPLIER', (done) => {
          //mock up a store
          const store = createMockStore({});

          const updates = {
            name: 'wow',
            rating: 500
          };

          store.dispatch(actions.startUpdateSupplier(testSupplierRef.key,
            updates)).then(() => {
              //collect fired actions in mock store
              const actions = store.getActions();

              //make assertions
              expect(actions[0]).toInclude({
                type: 'UPDATE_SUPPLIER'
              });

              expect(actions[0].supplier.name).toEqual(updates.name);
              done();
          }, done());
        });

        it('should  delete a supplier and dispatch DELETE_SUPPLIER', (done) => {
          //mock up a store to check actions that have been fired
          const store = createMockStore({});

          //dispatch some actions
          store.dispatch(actions.startDeleteSupplier(testSupplierRef.key)).then(() => {

            const actions = store.getActions();
            //assert that actions

            expect(actions[0]).toInclude('DELETE_SUPPLIER');

            expect(actions[0]).toExclude(supplier);
            done();
          }, done());
        })
      });

    describe('Service Line Actions', () => {
      describe('Add Service Line', () => {
        it('should generate ADD_SUPPLIER_SERVICE_LINE action', () => {
          var sl = {
            name: 'luxury room',
            category: 'accomodation',
            supplier: 'skcd234'
          }

          var action = {
            type: 'ADD_SUPPLIER_SERVICE_LINE',
            serviceLine: sl
          };

          var res = actions.addSupplierServiceLines(action.serviceLine);

          expect(res).toEqual(action);
        });
      });

      describe('UPDATE SERVICE LINE', () => {
        var action = {
          type: 'UPDATE_SERVICE_LINE',
          id:1,
          updates: {
            name: 'lux'
          }
        };

        var res = actions.updateServiceLine(action.id, action.updates);
        it('should generate UPDATE_SERVICE_LINE actio', () => {
          expect(res).toEqual(action);
        });
      });

      describe('DELETE SERVICE LINE', () => {
        it('should generate DELETE_SERVICE_LINE action', () => {
          var action = {
            type: 'DELETE_SERVICE_LINE',
            id: 1
          };

          var res = actions.deleteServiceLine(action.id);

          expect(res).toEqual(action);
        });
      });
    });

    describe('Service Line Async Tests', () => {
      //before each and after each cases
      var testLineRef;

      var testLine =  {
        name: 'my line',
        category: 'accom',
        supplier: 1234
      };

      //before each to clear db and add a sample for testing
      beforeEach((done) => {
        var serviceLinesRef = firebaseRef.child('service_lines');

        //wipe all lines
        serviceLinesRef.remove().then(() => {
          testLineRef = firebaseRef.child('service_lines').push();

          //load our sample to firebase
          return testLineRef.set(testLine);
        }).then(() => done())
        .catch(done);
      });

      //implement after each to clear the mess we just made
      afterEach((done) => {
        testLineRef.remove().then(() => done());
      })

      //test for adding supplier to db
      
    });

    describe('Policy Actions', () => {
      describe('Add Supplier Policy', () => {
        it('should generate ADD_SUPPLIER_POLICY action',() => {
          var policy = {
            supplier:'xkcd234',
            name: 'child policy',
            text: 'unattended children will be given sugar laden expressos'
          };

          var action = {
            type: 'ADD_SUPPLIER_POLICY',
            policy: policy
          };

          var res = actions.addSupplierPolicy(action.policy);

          expect(res).toEqual(action);
        });
      });

      describe('UPDATE policy', () => {
        var action = {
          type: 'UPDATE_POLICY',
          id: 1,
          updates: {
            name:'cancellation'
          }
        };

        var res = actions.updatePolicy(action.id, action.updates);
        it('should generate UPDATE_POLICY acion', () => {
          expect(res).toEqual(action);
        });
      });

      describe('DELETE POLICY', () => {
        it('should generare DELETE_POLICY action', () => {
          var action = {
            type: 'DELETE_POLICY',
            id: 1
          };

          var res = actions.deletePolicy(action.id);

          expect(res).toEqual(action);
        });
      });
    });

    describe('Rate Period Actions', () => {
      describe('Add Supplier Rate Period', () => {
        it('should generate ADD_SUPPLIER_RATE_PERIOD action', () => {
          var rateperiod = {
            name:'peak',
            start: '01 Jan',
            end:  '02 Jan',
            supplier: 'xkcd234'
          };

          var action = {
            type: 'ADD_SUPPLIER_RATE_PERIOD',
            period: rateperiod
          };

          var res = actions.addSupplierRatePeriod(action.period);

          expect(res).toEqual(action);
        });
      });

      describe('UPDATE Rate Period', () => {
        var action = {
          type: 'UPDATE_RATE_PERIOD',
          id: 1,
          updates: {
            start: '02 Jan'
          }
        };

        var res = actions.updateSupplierRatePeriod(action.id, action.updates);
        it('should generate UPDATE_RATE_PERIOD action',() => {
          expect(res).toEqual(action);
        });
      });

      describe('DELETE Rate Period', () => {
        it('should generate DELETE_RATE_PERIOD action', () => {
          var action = {
            type: 'DELETE_RATE_PERIOD',
            id: 2
          };

          var res = actions.deleteRatePeriod(action.id);

          expect(res).toEqual(action);
        });
      });
    });

    describe('Rate Cost Actions', () => {
      describe('Add Supplier Rate Cost', () => {
        it('should generate ADD_SUPPLIER_COSTS action', () => {
          var ratecost = {
            service_line: 'lux',
            rate_period: 'peak',
            type: 'pax',
            pps: 'pps cost',
            single: 'single cost',
            thirdadu: 'third adult cost',
            child: 'child rate cost'
          };

          var action = {
            type: 'ADD_SUPPLIER_COSTS',
            ratecost: ratecost
          };

          var res = actions.addSupplierCosts(action.ratecost);

          expect(res).toEqual(action);
        });
      });

      describe('UPDATE Rate Cost', () => {
        it('should generate UPDATE_COST action', () => {
          var action =  {
            type: 'UPDATE_COST',
            id: 1,
            updates: {
              type: 'rack'
            }
          };

          var res = actions.updateSupplierCost(action.id, action.updates);

          expect(res).toEqual(action);
        });
      });

      describe('DELETE Rate Cost', () => {
        it('should generare delete RATE Cost Action', () => {
          var action = {
            type: 'DELETE_COST',
            id: 2
          };

          var res = actions.deleteRateCost(action.id);

          expect(res).toEqual(action);
        });
      });
    });
});
