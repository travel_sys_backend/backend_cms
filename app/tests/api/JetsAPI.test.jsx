//Import dependencies for testing
import expect from 'expect';
var JetsAPI = require('app/api/JetsAPI');

//write assertions
describe('JetsAPI', () => {
  it('should exist', () => {
    expect(JetsAPI).toExist();
  });

  describe('filter current set of Items',  () => {
    var sls = [
      {
        name: 'luxury room',
        category: 'accomodation',
        supplier: 'skcd234'
      },
      {
        name: 'luxury room',
        category: 'accomodation',
        supplier: 'skcd234'
      },
      {
        name: 'luxury room',
        category: 'accomodation',
        supplier: 'xkcd234'
      }];

      it('should return servicelines for current supplier only', () => {
        var currentServiceLines = JetsAPI.currentSet(sls, 'xkcd234');

        expect(currentServiceLines.length).toBe(1);
      });
  });
});
