import expect from 'expect';
import moment from 'moment';

//add deep freeze to make sure our reducers must be pure functions
var df = require('deep-freeze-strict');
import * as reducers from 'app/reducers/reducers';

describe('Reducers', () => {
  it('should exist',  () => {
    expect(reducers).toExist();
  });

  describe('rateCostsReducer', () => {
    var ratecosts =  [
      {
        service_line: 'lux',
        rate_period: 'peak',
        type: 'pax',
        pps: 'pps cost',
        single: 'single cost',
        thirdadu: 'third adult cost',
        child: 'child rate cost',
        id:1
      },
      {
        service_line: 'lux',
        rate_period: 'peak',
        type: 'pax',
        pps: 'pps cost',
        single: 'single cost',
        thirdadu: 'third adult cost',
        child: 'child rate cost',
        id:2
      }];

      it('should delete a rate cost', () => {
        var action = {
          type: 'DELETE_COST',
          id: ratecosts[1].id
        };

        var res = reducers.rateCostsReducer(df(ratecosts), df(action));

        expect(res).toExclude(ratecosts[1]);
      });

      it('should update a rate cost', () => {
        var updates = {
          child: '300',
          single:'400'
        };

        var action = {
          type: 'UPDATE_COST',
          id: ratecosts[1].id,
          updates: updates
        };

        var res = reducers.rateCostsReducer(df(ratecosts), df(action));

        expect(res[1].child).toEqual(updates.child);
        expect(res[1].single).toEqual(updates.single);
      });

      it('should add a new rate cost', () => {
        var ratecost = {
          service_line: 'lux',
          rate_period: 'peak',
          type: 'pax',
          pps: 'pps cost',
          single: 'single cost',
          thirdadu: 'third adult cost',
          child: 'child rate cost'
        };

        var action = {
          type: 'ADD_SUPPLIER_COSTS',
          ratecost: ratecost
        };

        var res = reducers.rateCostsReducer(df([]), df(action));

        expect(res).toInclude(action.ratecost);
      });
  });

  describe('ratePeriodsReducer', () => {
    var periods = [
      {
        name: 'peak',
        start: '02 jan',
        end:  '04 jan',
        supplier: 'xkcd'
      },
      {
        name: 'loq',
        start: '02 jun',
        end:  '04 jun',
        supplier: 'xkcd'
      }
    ];

    it('should delete a rateperiod', () => {
      var action = {
        type: 'DELETE_RATE_PERIOD',
        id: periods[1].id
      };

      var res = reducers.ratePeriodsReducer(df(periods), df(action));

      expect(res).toExclude(periods[1]);
    });

    it('should update a rate period', () => {
      var updates = {
        end:'05 dec'
      };

      var action = {
        type:'UPDATE_RATE_PERIOD',
        id: periods[1].id,
        updates
      };

      var res = reducers.ratePeriodsReducer(df(periods), df(action));

      expect(res[1].end).toEqual(updates.end);
    });

    it('should add a new rate period', () => {
      var action = {
        type: 'ADD_SUPPLIER_RATE_PERIOD',
        period: {
          name: 'peak',
          start: '02 jan',
          end:  '04 jan',
          supplier: 'xkcd'
        }
      };

      var res = reducers.ratePeriodsReducer(df([]), df(action));

      expect(res.length).toEqual(1);
      expect(res).toInclude(action.period);
    });
  });

  describe('policyReducer', () => {
    var policies = [
      {
        name: 'child',
        supplier: 'xkcd',
        text: 'test',
        id:1
      },
      {
        name: 'child',
        supplier: 'xkcd',
        text: 'test',
        id:2
      }
    ];
    it('should add a new policy', () => {
      var policy = {
        name: 'child',
        supplier: 'xkcd',
        text: 'test'
      };

      var action = {
        type: 'ADD_SUPPLIER_POLICY',
        policy: policy
      };

      var res = reducers.policyReducer(df([]), df(action));

      expect(res).toInclude(action.policy);
    });

    it('should update a single policy', () => {
      var updates = {
        text: 'text'
      };

      var action = {
        type: 'UPDATE_POLICY',
        id: policies[1].id,
        updates
      };

      var res = reducers.policyReducer(df(policies), df(action));

      expect(res[1].text).toEqual(updates.text);
    });

    it('should delete a policy', () => {
      var action  = {
        type: 'DELETE_POLICY',
        id: policies[0].id
      };

      var res = reducers.policyReducer(df(policies), df(action));

      expect(res).toExclude(policies[0]);
    });
  });

  describe('serviceLineReducer', () => {
    it('should delete a service line', () => {
      var sls = [
        {
          name: 'luxury',
          id:1,
          category: 'accomodation',
          supplier: 'xkcd'
        },
        {
          name: 'not luxury',
          id:2,
          category: 'accomodation',
          supplier: 'xkcd'
        }];

        var action = {
          type:'DELETE_SERVICE_LINE',
          id: sls[1].id
        };

        var res = reducers.serviceLineReducer(df(sls), df(action));

        expect(res).toExclude(sls[1]);
    });
    it('should add a service line', () => {
      var sl = {
        name: 'luxury',
        category: 'accomodation',
        supplier: 'xkcd'
      };

      var action = {
        type:'ADD_SUPPLIER_SERVICE_LINE',
        serviceLine:sl
      };

      var res = reducers.serviceLineReducer(df([]), df(action));

      expect(res.length).toEqual(1);
    });

    it('should update a service line', () => {
      var sls = [
        {
          name: 'luxury',
          id:1,
          category: 'accomodation',
          supplier: 'xkcd'
        },
        {
          name: 'not luxury',
          id:2,
          category: 'accomodation',
          supplier: 'xkcd'
        }];

        var updates= {
          category:'transfer'
        };

        var action = {
          type:'UPDATE_SERVICE_LINE',
          id: sls[1].id,
          updates
        };

        var res = reducers.serviceLineReducer(df(sls), df(action));

        expect(res[1].category).toEqual(updates.category);
    });
  });

  describe('currSupplierReducer', () => {
    it('should set the current supplier', () => {
      var action = {
        type:'SET_CURR_SUPPLIER',
        key: 'xkcd'
      };

      var res = reducers.currSupplierReducer(df(''), df(action));

      expect(res).toEqual(action.key);
    });
  });

  describe('supplierReducer', () => {
    it('should delete a supplier',() => {
      var suppliers = [
        {
          id: '23243',
          name: 'new supplier',
          fact_sheet: 'fact sheet',
          liability_sheet: 'liability sheet',
          group: 'group',
          service_lines: null,
          policies: null,
          rate_periods: null,
          rate_costs: null,
          contracts: null,
          cancellation_terms: null,
          deposit_terms: null,
          commission: '22',
          creation_date: moment().unix(),
          country: 'SA',
          region: 'Capetown',
          email: 'mail@m.com',
          telephone: '2797179',
          website: 'ww.ww.ww',
          category: 'cat',
          rating: '100'
        }];

        var action = {
          type:'DELETE_SUPPLIER',
          key: suppliers[0].id
        };

        var res = reducers.supplierReducer(df(suppliers), df(action));

        expect(res).toExclude(suppliers[0]);
    });
    it('should update a supplier', () => {
      var suppliers = [
        {
          id: '23243',
          name: 'new supplier',
          fact_sheet: 'fact sheet',
          liability_sheet: 'liability sheet',
          group: 'group',
          service_lines: null,
          policies: null,
          rate_periods: null,
          rate_costs: null,
          contracts: null,
          cancellation_terms: null,
          deposit_terms: null,
          commission: '22',
          creation_date: moment().unix(),
          country: 'SA',
          region: 'Capetown',
          email: 'mail@m.com',
          telephone: '2797179',
          website: 'ww.ww.ww',
          category: 'cat',
          rating: '100'
        }];

        var updates = {
          rating: '34',
          category: 'dog'
        };

        var action =  {
          type: 'UPDATE_SUPPLIER',
          id: suppliers[0].id,
          updates
        };

        //call reducers
        var res = reducers.supplierReducer(df(suppliers), df(action));

        //assertions
        expect(res[0].category).toEqual(updates.category);
        expect(res[0].rating).toEqual(updates.rating);
        expect(res[0].id).toEqual(suppliers[0].id);

    });
    it('should add new supplier',  () => {
      var supplier = {
        name: 'new supplier',
        fact_sheet: 'fact sheet',
        liability_sheet: 'liability sheet',
        group: 'group',
        service_lines: null,
        policies: null,
        rate_periods: null,
        rate_costs: null,
        contracts: null,
        cancellation_terms: null,
        deposit_terms: null,
        commission: '22',
        creation_date: moment().unix(),
        country: 'SA',
        region: 'Capetown',
        email: 'mail@m.com',
        telephone: '2797179',
        website: 'ww.ww.ww',
        category: 'cat',
        rating: '100'
      };

      var action = {
        type:'ADD_SUPPLIER',
        supplier: supplier
      };

      //make assertion and call reducer
      var res = reducers.supplierReducer(df([]), df(action));
      expect(res.length).toEqual(1);
    });
  });
});
