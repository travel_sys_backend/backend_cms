//firebase to save
import firebase, {firebaseRef} from 'app/firebase/index';
//3rd party libs
import moment from 'moment';
//auth stuff
export var login = () => {
  type: 'LOGIN'
}

//supplier info actions
export var addSupplier = (supplier) => {
  return {
    type: 'ADD_SUPPLIER',
    //take in object for usage locally
    supplier
  };
}

//supplier update action
export var updateSupplier = (id, updates) => {
  return {
    type:'UPDATE_SUPPLIER',
    id,
    updates
  };
}

export var startUpdateSupplier = (id, updates) => {
  return (dispatch, getState) => {

  };
}

export var deleteSupplier = (id) => {
  return {
    type:'DELETE_SUPPLIER',
    key:id
  };
}

export var startDeleteSupplier = (id) => {
  return (dispatch, getState) => {

  };
}

//local state element current supplier for context
export var currSupplier = (key) => {
  return{
    type: 'SET_CURR_SUPPLIER',
    key
  }
};



//thunked action generator for asnyc
export var startAddSupplier = (name, fact_sheet, liability_sheet, group,
  commission,country,region,email,telephone, website,category,rating) => {
  return (dispatch, getstate) => {
    var supplier = {
      name,
      fact_sheet,
      liability_sheet,
      group,
      service_lines: null,
      policies: null,
      rate_periods: null,
      rate_costs: null,
      contracts: null,
      cancellation_terms: null,
      deposit_terms: null,
      commission,
      creation_date: moment().unix(),
      country,
      region,
      email,
      telephone,
      website,
      category,
      rating
    };

    /*add supplier to firebase*/
    var supplierRef = firebaseRef.child('suppliers').push(supplier);

    /*sync with firebase*/
    return supplierRef.then(() =>  {
      dispatch(addSupplier({
        ...supplier,
        id: supplierRef.key
      }));
      dispatch(currSupplier(supplierRef.key));
    });
  }
};



export var addSupplierServiceLines = (serviceLine) => {
  return {
    type:'ADD_SUPPLIER_SERVICE_LINE',
    serviceLine
  }
};

export var startServiceLine  = (name, category, currentSupplier) => {
  return (dispatch, state) => {
    //construct
    var serviceLine = {
      supplier: currentSupplier,
      category,
      name,
    }

    /*post to hell*/
    var serviceLineRef = firebaseRef.child('service_lines').push(serviceLine);
    return serviceLineRef.then(() => {
      dispatch(addSupplierServiceLines({
        ...serviceLine,
        id: serviceLineRef.key
      }));
    });
  };
}

//update action for service Line
export var updateServiceLine  = (id, updates) => {
  return {
    type: 'UPDATE_SERVICE_LINE',
    id,
    updates
  };
}

export var startUpdateServiceLine =  (id, updates) => {
  return (dispatch, getState) => {

  };
}

export var deleteServiceLine =  (id) => {
  return {
    type: 'DELETE_SERVICE_LINE',
    id
  };
}

export var startDeleteServiceLine = (id) => {
  return (dispatch, getState) => {

  };
}

export var addSupplierPolicy = (policy) => {
  return {
    type: 'ADD_SUPPLIER_POLICY',
    policy
  };
}

export var startPolicy = (name, text, currentSupplier) => {
  return(dispatch, state) =>  {
    /*construct*/
    var policy = {
      supplier: currentSupplier,
      name,
      text
    };
    /*Send to hell*/
    var policyRef = firebaseRef.child('policies').push(policy);

    return policyRef.then(() => {
      dispatch(addSupplierPolicy({
        ...policy,
        id: policyRef.key
      }));
    });
  };
}

//action to update policy
export var updatePolicy = (id, updates) => {
  return{
    type: 'UPDATE_POLICY',
    id,
    updates
  };
}

export var startUpdatePolicy = (id, updates) => {
  return(dispatch, getState) => {

  };
}

export var deletePolicy = (id) => {
  return {
    type: 'DELETE_POLICY',
    id
  };
}

export var startDeletePolicy = (id) => {
  return(dispatch, getState) => {

  };
}


export var addSupplierRatePeriod = (period) => {
  return {
    type: 'ADD_SUPPLIER_RATE_PERIOD',
    period
  };
}

export var startPeriod  = (name, start, end, currentSupplier) => {
  return (dispatch, state) => {
    var period = {
      name,
      start,
      end,
      supplier: currentSupplier
    };
    /*Send to hell*/
    var periodRef = firebaseRef.child('rate_periods').push(period);

    return periodRef.then(() => {
      dispatch(addSupplierRatePeriod({
        ...period,
        id: periodRef.key
      }));
    });
  };
}

export var updateSupplierRatePeriod = (id, updates) => {
  return {
    type: 'UPDATE_RATE_PERIOD',
    id,
    updates
  };
}

export var startUpdateSupplierRatePeriod = (id, updates) => {
  return (dispatch, getState) => {

  };
}

export var deleteRatePeriod = (id) => {
  return {
    type: 'DELETE_RATE_PERIOD',
    id
  }
}

export var startDeleteRatePeriod = (id) => {
  return (dispatch, getState) => {

  };
}


export var addSupplierCosts = (ratecost) => {
  return {
    type:'ADD_SUPPLIER_COSTS',
    ratecost
  };
}

export var startRateCost =  (sl, rp, type, pps, single, thirdadu, child,supplier) => {
  return (dispatch, state) => {
    var ratecost = {
      service_line: sl,
      rate_period: rp,
      type,
      pps,
      single,
      thirdadu,
      child,
      supplier
    }

    /*submit to hell */
    var costRef = firebaseRef.child('rate_costs').push(ratecost);

    return costRef.then(() => {
      dispatch(addSupplierCosts({
        ...ratecost,
        id: costRef.key
      }));
    });
  }
}


export var updateSupplierCost = (id, updates) => {
  return {
    type:'UPDATE_COST',
    id,
    updates
  };
}

export var startUpdateSupplierCost = (id, updates) => {
  return(dispatch, getState) => {

  };
}

export var deleteRateCost = (id) => {
  return {
    type: 'DELETE_COST',
    id
  };
}

export var startDeleteRateCost = (id) => {
  return (dispatch, getState) => {

  };
}


//action to load existing suppliers
//TODO lazy load suppliers, do pagination
export var loadSuppliers  = (suppliers) => {
    return  {
      type: 'LOAD_SUPPLIERS',
      suppliers
    }
}

//action generator suppliers
export var startLoadSuppliers = () => {
    /*fetch suppliers from firebase*/
    //TODO pagination here ?
    return(dispatch, state) => {
      var suppliersRef = firebaseRef.child('suppliers');

      //using once to collect values
      //TODO switch to fetching a snapshshot everytime suppliers change
      //TODO add UI indicator for fectching
      return suppliersRef.once('value').then((snapshot) => {
        //consume but first check if empty
        var suppliers = snapshot.val() || {};
        var parsedSuppliers = [];

        /*convert from object to array*/
        Object.keys(suppliers).forEach((suppId) => {
          parsedSuppliers.push({
            id:suppId,
            ...suppliers[suppId]
          });
        });

          /*give the redux store our fetch suppliers */
          dispatch(loadSuppliers(parsedSuppliers));
        })
      };
}
