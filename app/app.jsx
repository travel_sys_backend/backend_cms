//import react
import React from 'react';
import ReactDOM from 'react-dom'
//Add Redux components and use them
import {Provider} from 'react-redux'

var JetsAPI = require('app/api/JetsAPI');
//Import Actions
var actions = require('app/actions/actions');
//Add a store
var store = require('app/store/configureStore').configure();
//import router
import router from 'app/router/index'


//add inject plugin for material-ui
import injectTapEventPlugin from 'react-tap-event-plugin';
//start inject pluging
injectTapEventPlugin();

// load jquery and foundation in the window scope
import 'script!jquery'
import 'script!what-input'
import 'script!foundation-sites'
//app css require
require('style!css!sass!applicationStyles');


import firebase from 'app/firebase/index';
//enable firebase logging
//firebase.database.enableLogging(true);

//let us attempt to start the cluster
var elasticsearch = require('elasticsearch'),
  conf = require('../config'),
  fbutil = require('../lib/fbutil'),
  PathMonitor = require('../lib/PathMonitor'),
  SearchQueue = require('../lib/SearchQueue');

var escOptions = {
  hosts: [{
    host: conf.ES_HOST,
    port: conf.ES_PORT,
    auth: (conf.ES_USER && conf.ES_PASS) ? conf.ES_USER + ':' + conf.ES_PASS : null,
    log: 'trace'
  }]
};

for (var attrname in conf.ES_OPTS) {
  if( conf.ES_OPTS.hasOwnProperty(attrname) ) {
    escOptions[attrname] = conf.ES_OPTS[attrname];
  }
}

// connect to ElasticSearch
var esc = new elasticsearch.Client(escOptions);

console.log('Connecting to ElasticSearch host %s:%s'.grey, conf.ES_HOST, conf.ES_PORT);

var timeoutObj = setInterval(function() {
  esc.ping()
    .then(function() {
      console.log('Connected to ElasticSearch host %s:%s'.grey, conf.ES_HOST, conf.ES_PORT);
      clearInterval(timeoutObj);
      initFlashlight();
    });
}, 5000);

function initFlashlight() {
  console.log('Connecting to Firebase %s'.grey, conf.FB_URL);
  fbutil.init(conf.FB_URL, conf.FB_SERVICEACCOUNT);
  PathMonitor.process(esc, conf.paths, conf.FB_PATH);
  SearchQueue.init(esc, conf.FB_REQ, conf.FB_RES, conf.CLEANUP_INTERVAL);
}

//end cluster work



//run sample query
JetsAPI.searchDB('Kaya');

//TODO do this using the  a childs lifecycle
//store.dispatch(actions.startLoadSuppliers());
//Create our Router
ReactDOM.render(    //pass two args, JSX and the app element
  /*Add the redux provider so our connectec components can call dispatch*/
  <Provider store={store}>
    {router}
  </Provider>,
  document.getElementById('app') //where to render
);
