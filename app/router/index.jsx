import React from 'react';
import {Route, Router, IndexRoute, hashHistory, IndexRedirect} from 'react-router';
import LandingPage from 'LandingPage';



//add components these are pages needed for routing
var Jets = require('Jets');
var LoginPage = require('LoginPage');
var SignUpPage = require('SignUpPage');
var GenericDashboard = require('GenericDashboard');
var ProfilePage = require('ProfilePage');
var Specials = require('Specials');
var Services = require('Services');
var FullService  = require('FullService');
var ServiceLineInfo = require('ServiceLineInfo');
var ServiceRates = require('ServiceRates');
var ServicePolicies = require('ServicePolicies');
var Product = require('Product');
//product children
var AddRates = require('AddRates');
var BuildItinerary = require('BuildItinerary');
var ItineraryPrimary = require('ItineraryPrimary');
var ItinerarySecondary = require('ItinerarySecondary');

//export router

export default (
  <Router history={hashHistory}>
    {/*Main component will always be rendered*/}
    <Route path="/" component={Jets}>
      {/*Add Routes here*/}
    <IndexRoute component={LandingPage}/>
    <Route path="serviceitem" component={FullService}>
      <IndexRedirect to="/serviceitem/slinfo"/>
      <Route path="slinfo" component={ServiceLineInfo}/>
      <Route path="slrates" component={ServiceRates}/>
      <Route path="slpols" component={ServicePolicies}/>
    </Route>
    <Route path ="product" component={Product}>
      <IndexRedirect to="/product/build"/>
      <Route path="build" component={BuildItinerary}/>
      <Route path="primary" component={ItineraryPrimary}/>
      <Route path="secondary" component={ItinerarySecondary}/>
      <Route path="rates"  component={AddRates}/>
    </Route>
    <Route path="services" component={Services}/>
    <Route path="specials" component={Specials}/>
    <Route path="generic" component={GenericDashboard}/>
    <Route path="profile" component={ProfilePage}/>
    <Route path="signup" component={SignUpPage}/>
    <Route path="login" component={LoginPage}/>
    </Route>
  </Router>
)
