




Travel Backend System
========

This is a travel backend system to help companies Research and build trips with ease.
Rebased and moved to
REACT for FrontEnd Development.
Firebase for DataStorage.
NodeJs for Backend Processing.

Look how easy it is to use:

    download or clone the source
    The project requires NodeJs Version 6.0.0 and above work
    run `npm i`
    run `npm start`
    rebuilding node-sass so the app can work on heroku 


Features
--------

- Record Supplier Information.
-

Installation
------------

Install $project by running:

    `npm i` or `npm install`

Contribute
----------

- Source Code: https://bitbucket.org/account/user/travel_sys_backend/projects/BC

Support
-------

If you are having issues, please let a developer know.


License
-------

The project is licensed under the BSD license.
